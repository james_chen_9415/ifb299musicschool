var id = [];
var arr = [];
var availableLanguage = [];
var counter = 0;

$(document).ready(function(){

	// find out the set of available language, item appears more than one time will only be treated one time
	$(".table-list-search td.data-lang").each(function(){
		id.push( $(this).find("div").attr("for") );
		arr.push( $(this).find("div").attr("data") );
		$(this).find("div").text(arr[counter]);

		var temp = arr[counter].split(',');

		var i = 0;

		while (temp[i] != null) {
			if (availableLanguage.indexOf(temp[i]) < 0) {
				availableLanguage.push(temp[i].trim());
			}
			i++;
		}

		counter++;
	});

	// building the option list for language filter.
	for (i = 1; i < availableLanguage.length; i++) {
		$("#filter-option").append("<label class=\"checkbox-inline\"><input name=\"filter-keyword\" type=\"checkbox\" value=\""+availableLanguage[i]+"\">"+availableLanguage[i]+"</label>");
	}
	
	// process filtering
	$("#filter-submit").on("click", function(e){
		e.preventDefault();
		$(".table-list-search td.data-lang").each(function(){
			$(this).parent().hide();
		});
		$("input[name=filter-keyword]:checked").each(function(){
			var  keyword = $(this).val();
			$(".table-list-search td.data-lang").each(function(){
				var dataString = $(this).find("div").attr("data");
				if (dataString.includes(keyword)) {
					$(this).parent().show();
				}
			});
		});
	});
});