$(document).ready(function(){
	$(".write-feedback-btn").on("click", function(){
    /* This function will get the record data from this row and copy the value to the feedback form */
		$("#feedback-form input[name=class]").val($(this).attr("data-id"));
		$("#feedback-form input[name=student]").val($(this).attr("data-from"));
	});
});

function send_feedback() {
  try{
    $.ajax({
      type: 'post',
      url: $("#feedback-form").attr("action"),
      data: {
        'classid': $("#feedback-form input[name=class]").val(),
        'subject': $("#feedback-form input[name=subject]").val(),
        'studentid': $("#feedback-form input[name=student]").val(),
        'feedback': $("#feedback-form textarea[name=feedback]").val()
      },
      success: function(response, status, xhr) {
        try{
          data = $.parseJSON(xhr.responseText);

          if (data.status == "success") {
            // clear the feedback form
          	$("#feedback-form")[0].reset();

            // show the result model
          	$('#myModalResult').modal('show');
          }
        } catch(e){
          console.log("Error on ajax success.\n"+e.message);
        }
      },
      error: function(response, status, xhr){
        alert("There is an error on submitting the enquiry, please try it again!");
      }         
    }); 
  } catch(e){
    console.log("Error on ajax process.\n"+e.message);
  }
}