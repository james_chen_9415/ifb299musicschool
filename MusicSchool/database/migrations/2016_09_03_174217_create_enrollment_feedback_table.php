<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollmentFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrollmentFeedback', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enrollmentId')->unsigned();
            $table->integer('studentId')->unsigned();
            $table->string('subject');
            $table->string('feedback');

            $table->foreign('enrollmentId')->references('id')->on('enrollment')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enrollmentFeedback');
    }
}
