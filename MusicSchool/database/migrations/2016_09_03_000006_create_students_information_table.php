<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studentInfos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('studentNum')->unique();
            $table->integer('userId')->unsigned();
            $table->string('firstName');
            $table->string('lastName');
            $table->tinyInteger('gender',false,true);
            $table->string('phoneNum');
            $table->date('DOB');
            $table->string('address');
            $table->integer('parentsId')->unsigned();
            $table->tinyInteger('isNew');
            $table->string('FacebookId');
        });

        Schema::table('studentInfos', function(Blueprint $table) {
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            //$table->foreign('parentsId')->references('id')->on('parentsInfo')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('studentInfos');
    }
}
