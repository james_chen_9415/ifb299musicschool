<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacherInfos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('teacherNum')->unique();
            $table->integer('userId')->unsigned();
            $table->string('firstName');
            $table->string('lastName');
            $table->tinyInteger('gender',false,true);
            $table->integer('phoneNum');
            $table->date('DOB');
            $table->string('address');
            $table->tinyInteger('isHired');
            $table->string('FacebookId');
        });

        Schema::table('teacherInfos', function(Blueprint $table) {
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('teacherInfos');
    }
}
