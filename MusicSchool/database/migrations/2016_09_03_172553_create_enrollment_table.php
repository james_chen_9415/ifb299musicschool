<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrollment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('studentId')->unsigned();
            $table->integer('teacherId')->unsigned();
            $table->integer('skillId')->unsigned();
            $table->integer('level');
            $table->string('room');
            $table->date('startDate');
            $table->date('endDate');
            $table->string('weekday');
            $table->time('time');
            $table->char('mark', 1);
            $table->tinyInteger('withPermission');
            $table->integer('tuitionFee');

            $table->foreign('teacherId')->references('teacherNum')->on('teacherInfos')->onDelete('cascade');
            $table->foreign('studentId')->references('studentNum')->on('studentInfos')->onDelete('cascade');
            //$table->foreign('skillId')->references('id')->on('skills')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enrollment');
    }
}
