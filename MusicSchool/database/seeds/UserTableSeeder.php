<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        User::create([
            'name' => 'James',
            'email' => 'james.chen.9415@gmail.com',
            //'password' => \Hash::make('hacker_35'),
            'password' => bcrypt('hacker_35'),

        ]);
    }
}
