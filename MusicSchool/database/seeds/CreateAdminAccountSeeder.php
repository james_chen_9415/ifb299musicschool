<?php

use Illuminate\Database\Seeder;
use App\User;

class CreateAdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'admin@pinelandsmusic.com.au',
            'role' => 'admin',
            'password' => bcrypt('admintest'),

        ]);
    }
}
