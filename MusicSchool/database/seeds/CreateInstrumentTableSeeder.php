<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Schema\Blueprint;
use App\Instrument;

class CreateInstrumentTableSeeder extends Seeder
{


    private function insertDummyData() {
        Instrument::create([
            'category' => 'Cello',
            'info' => 'Cello blah blah blah',
            'cost' => 30,
            'hired_to' => null,
            'hire_date' => null,
        ]);

        Instrument::create([
            'category' => 'Piano',
            'info' => 'Piano blah blah blah',
            'cost' => 100,
            'hired_to' => null,
            'hire_date' => null,
        ]);

        Instrument::create([
            'category' => 'Cello',
            'info' => 'Cello blah blah blah',
            'cost' => 40,
            'hired_to' => null,
            'hire_date' => null,
        ]);

        Instrument::create([
            'category' => 'Cello',
            'info' => 'Cello blah blah blah',
            'cost' => 35,
            'hired_to' => null,
            'hire_date' => null,
        ]);

        Instrument::create([
            'category' => 'Piano',
            'info' => 'Piano blah blah blah',
            'cost' => 100,
            'hired_to' => null,
            'hire_date' => null,
        ]);

        Instrument::create([
            'category' => 'Guitar',
            'info' => 'Guitar blah blah blah',
            'cost' => 30,
            'hired_to' => null,
            'hire_date' => null,
        ]);

        Instrument::create([
            'category' => 'Guitar',
            'info' => 'Guitar blah blah blah',
            'cost' => 34,
            'hired_to' => null,
            'hire_date' => null,
        ]);

        Instrument::create([
            'category' => 'Percussion',
            'info' => 'Percussion blah blah blah',
            'cost' => 50,
            'hired_to' => null,
            'hire_date' => null,
        ]);

        Instrument::create([
            'category' => 'Percussion',
            'info' => 'Percussion blah blah blah',
            'cost' => 55,
            'hired_to' => null,
            'hire_date' => null,
        ]);

        Instrument::create([
            'category' => 'Percussion',
            'info' => 'Percussion blah blah blah',
            'cost' => 60,
            'hired_to' => null,
            'hire_date' => null,
        ]);



    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Schema::hasTable('instruments')) {
            Schema::create('instruments', function (Blueprint $table) {
                $table->engine = 'InnoDB';

                $table->increments('id');
                $table->string('category', 30);
                $table->text('info')->nullable();
                $table->float('cost');
                $table->date('hire_date')->nullable();
                $table->integer('hired_to')->unsigned()->nullable();
            });

            Schema::table('instruments', function(Blueprint $table) {
                $table->foreign('hired_to')->references('id')->on('users');
            });
        }

        $this->insertDummyData();
    }
}
