<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnrollmentFeedback extends Model
{
    protected $table = 'enrollmentFeedback';
    public $timestamps = false;
}
