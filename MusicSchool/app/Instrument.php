<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instrument extends Model
{
    protected $table = 'instruments';
    public $primaryKey = 'id';
    public $timestamps = false;
}
