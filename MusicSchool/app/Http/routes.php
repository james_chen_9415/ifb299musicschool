<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contactus', function () {
    return view('contactus');
});

Route::get('/aboutus', function () {
    return view('aboutus');
});

// Authetication
Route::get('/login', ['as'=>'login', 'uses'=>'AuthController@login']);
Route::get('/logout', ['as'=> 'logout', 'uses'=> 'AuthController@logout']);
Route::post('/handleLogin', ['as'=>'handleLogin', 'uses'=>'AuthController@handleLogin']);

// Member Portal
Route::resource('/users', 'UserController', ['Only'=> ['create', 'store']]);
Route::get('/register', 'UserController@create');   
Route::resource('/profile/edit', 'UserController', ['Only'=> ['edit', 'update']]);

Route::group(['middleware' => 'auth'], function() {
	Route::get('/home', ['as'=> 'home', 'uses'=> 'UserController@home']);
	Route::get('/profile', ['as'=>'profile', 'uses'=> 'UserController@profile']); 
    Route::get('/profile/edit', ['as'=>'profile', 'uses'=> 'UserController@profileEdit']);
	Route::post('handleProfileUpdate', ['as'=>'handleProfileUpdate', 'uses'=>'UserController@handleProfileUpdate']);
});

// Lesson
Route::get('/lesson', 'LessonController@listRec');
Route::get('/lesson/singingclass', 'LessonController@listSingingClasses');
Route::get('/lesson/classespiano', 'LessonController@listPianoClasses');
Route::get('/lesson/classesdrums', 'LessonController@listDrumsClasses');
Route::get('/lesson/classesmusictheory', 'LessonController@listMusicTheoryClasses');
Route::resource('/lesson/enrollment', 'LessonController',['Only'=> ['create', 'store']]);
//Route::post('/lesson/handleEnrollment', ['as'=>'handleEnrollment', 'uses'=>'LessonController@handleEnrollment']);
Route::get('/lesson/apply', ['as'=> 'apply', 'uses'=> 'UserController@applyLessons']);
Route::patch('/lesson/applyHandle/{applyHandle}', ['as'=> 'applyHandle', 'uses'=> 'UserController@applyHandle']);

// Career
Route::get('/career', 'CareerController@recruit');




// For Release 2

// student part

Route::post('/student/handleFeedback', ['as'=>'handleFeedback', 'uses'=>'UserController@handleFeedback']);
Route::get('/student/historyMarks/feedbackToTeacher', function () {
    return view('users.student.feedbackToTeacher');
});
Route::group(['middleware' => 'auth'], function() {
    Route::get('/student/hireInstrument', function () {
        return view('users.student.hireInstrument');
    });
    Route::get('/student/historyMarks', ['as'=> 'historyMarks', 'uses'=> 'UserController@historyMarks']);
    Route::resource('/student/query-instrumentlist', "InstrumentController@query");
    Route::resource('/student/query-hireInstrument', "InstrumentController@hireInstrument");
});

// teachers part
Route::get('/teachers/myTimetable', function () {
    return view('users.teacher.myTimetable');
});
Route::get('/teachers/myTimetable/timetableArrangement', function () {
    return view('users.teacher.timetableArrangement');
});
Route::get('/teacher/mailBox', function () {
    return view('users.teacher.mailBox');
});

// admin part

Route::group(['middleware' => 'auth'], function() {
    Route::get('/admin', ['as'=> 'home', 'uses'=> 'UserController@home']);
    Route::get('/admin/instrumentManagement', "AdminController@viewInstruments");
    Route::post('/admin/instrumentManagement', ['as'=> 'addNewInstruments', 'uses'=>'AdminController@addNewInstruments']);
    Route::get('/admin/lessonArrangement', function () {
        return view('users.admin.lessonArrangement');
    });
    Route::get('/admin/enrollmentArrangement', "AdminController@enrollmentArrangement");
    Route::get('/admin/enrollmentArrangement/edit', function () {
        return view('users.admin.editEnrollmentForm');
    });
    Route::get('/admin/roomArrangement', function () {
        return view('users.admin.roomArrangement');
    });
    Route::get('/admin/mailBox', function () {
        return view('users.admin.mailBox');
    });

    Route::get('/admin/crewManagement/teachers/view', 'AdminController@viewTeachers');
    Route::get('/admin/crewManagement/teachers/add', function () {
        return view('users.admin.crewManagement.teachers.add');
    });
    Route::get('/admin/crewManagement/teachers/edit', function () {
        return view('users.admin.crewManagement.teachers.edit');
    });
    Route::get('/admin/crewManagement/teachers/viewFeedback', function () {
        return view('users.admin.crewManagement.teachers.viewFeedback');
    });

    Route::get('/admin/crewManagement/students/view', 'AdminController@viewStudents');
    Route::get('/admin/crewManagement/students/add', function () {
        return view('users.admin.crewManagement.students.add');
    });
    Route::get('/admin/crewManagement/students/edit', function () {
        return view('users.admin.crewManagement.students.edit');
    });

});

// owner part
Route::get('/owner', function () {
    return view('users.owner.home');
});Route::get('/owner/mailBox', function () {
    return view('users.owner.mailBox');
});
Route::get('/owner/mailHyperlink', function () {
    return view('users.owner.mailHyperlink');
});
Route::get('/owner/makeAnnouncement', function () {
    return view('users.owner.makeAnnouncement');
});




