<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Instrument;

/**
 *  The InstrumentController class performs all the backend activities related to hiring instruments.
 */
class InstrumentController extends Controller
{

    /**
     * This method return a list of the instrument records as a json object.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function query(Request $request)
    {
        $order = $request->input('orderby');

        if ($order == null){
            $model = Instrument::whereNull("hired_to")->get();
        } else {
            if ($order == 'cost') {
                $model = Instrument::orderBy('cost')->get();
            } else if ($order == 'instrument') {
                $model = Instrument::orderBy('category')->get();
            }
        }

        return response()->json($model);
    }

    /**
     * This method update the hire status of a instrument.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function hireInstrument(Request $request)
    {
        $booking_id = $request->input('hireid');
        $user = $request->input('user');

        if ($booking_id == null){
            return response()->json((['status' => 'false']));
        } else {
            date_default_timezone_set('UTC');
            $hire_date = date('Y-m-d');
            
            $updateRec = Instrument::find($booking_id);
            if (($updateRec->hired_to == null) || ($updateRec->hired_to == "")) {
                $updateRec->hired_to = $user;
                $updateRec->hire_date = $hire_date;
                $updateRec->save();
            } else {
                return response()->json((['status' => 'false']));
            }
        }

        return response()->json((['status' => 'true']));
    }
    
}
