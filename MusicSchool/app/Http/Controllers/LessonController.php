<?php

namespace App\Http\Controllers;

use App\TeacherInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Enrollment;
use App\StudentInfo;
use Carbon\Carbon;

/**
 *  The LessonController class performs all the backend activities related to lesson records.
 */
class LessonController extends Controller
{
    /**
     * This method displays a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Not planned to implement
    }

    /**
     * This method shows the form for enrolling to a new lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role == "student") {
            return view('/lesson/enrollment');
        } else{
            echo "Cannot enroll, because you are not student";
            //return back();
        }
    }

    /**
     * This method stores a new lesson enrollment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = Auth::user()->id;
        $student = StudentInfo::where('userId', $userId)->first();

        // Create user login
        $data = $request->all();
        $enrollment = new Enrollment;


        // check the ending date is bigger than starting date
        if(($data['startingDate'] <  $data['endingDate']) != 1){
            return back()->withInput()->withErrors("The ending date must be bigger than starting date");
        }

        // check the ending date is bigger than starting date by 7
        $startingDate = new Carbon($data['startingDate']);
        $endingDate = new Carbon($data['endingDate']);
        if($endingDate->diff($startingDate)->days <= 7){
            return back()->withInput()->withErrors("The ending date must be bigger than starting date by 7");
        }

        //check whether student have book at this time
        $checkElement = Enrollment::where('studentId', $student['studentNum'])->get()->where('weekday',$data['weekDay'])->where('time',$data['time']);
        if(count($checkElement) >= 1){
            // the student has already had a certain class at this time
            foreach($checkElement as $row){
                $oldStartDate = $row['startDate'];
                $oldEndDate = $row['endDate'];
                $newStartDate = $data['startingDate'];
                $newEndDate = $data['endingDate'];
                // the new and old tuition period is coincident
                if(($oldStartDate < $newStartDate && $oldEndDate > $newStartDate) || ($newStartDate < $oldStartDate && $newEndDate > $oldEndDate)){
                    return back()->withInput()->withErrors("You already have had a class in this time");
                }
            }
        }

        $skillsId = \DB::table('skills')
                    ->select('id')
                    ->where('skill','=',$data['instrumentClass'])
                    ->get();

        $enrollment->studentId = $student['studentNum'];
        $enrollment->teacherId = $data['teacherNum'];
        $enrollment->skillId = $skillsId[0]->id;
        $enrollment->level = $data['level'];
        $enrollment->room = $data['room'];
        $enrollment->startDate = $data['startingDate'];
        $enrollment->endDate = $data['endingDate'];
        $enrollment->weekday = $data['weekDay'];
        $enrollment->time= $data['time'];
        $enrollment->withPermission = "1";
        $enrollment->tuitionFee =$data['tuitionFee'];

        if($enrollment->save()){
            return redirect()->back()->with('success', 'Enrol successfully');
        }else{
            return back()->withInput()->withErrors("Not successfully");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Not planned to implement

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Not planned to implement

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Not planned to implement
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Not planned to implement
    }

    /**
     * This method redirect to another static content.
     *
     * @return \Illuminate\Http\Response
     */
    public function listRec(){
        return view('lesson.list');
    }

    /**
     * This method redirect to another static content.
     *
     * @return \Illuminate\Http\Response
     */
    public function listSingingClasses(){
        return view('lesson.classes');
    }

    /**
     * This method redirect to another static content.
     *
     * @return \Illuminate\Http\Response
     */
    public function listPianoClasses(){
        return view('lesson.classespiano');
    }

    /**
     * This method redirect to another static content.
     *
     * @return \Illuminate\Http\Response
     */
    public function listDrumsClasses(){
        return view('lesson.classesdrums');
    }

    /**
     * This method redirect to another static content.
     *
     * @return \Illuminate\Http\Response
     */
    public function listMusicTheoryClasses(){
        return view('lesson.classesmusictheory');
    }

}
