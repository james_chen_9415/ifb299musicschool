<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

/**
 *  The AuthController class performs all the backend activities related to authentication.
 */
class AuthController extends Controller
{
    /**
     * This method redirects the request to the static view of login page.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        return view('auth.login');
    }

    /**
     * This method checks if the user is a registered one and validate the login data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function handleLogin(Request $request){
        $this->validate($request, User::$login_validation_rules);

        $data = $request->only('email', 'password');
        if(\Auth::attempt($data)){
            return redirect()-> intended('home');
        }
        return back()->withInput()->withErrors(['email'=> 'Email or password is invalid']);
    }

    /**
     * This method closes the user login session.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(){
        \Auth::logout();
        return redirect()->route('login');
    }
}
