<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\StudentInfo;
use APP\TeacherInfo;
use APP\Enrollment;
use App\User;
use APP\Instrument;

/**
 *  The AdminController class performs all the backend activities related to administrator works.
 */
class AdminController extends Controller
{

    /**
     * This method lists out all the students' records.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewStudents(){

        $allstudents = \DB::table('studentInfos')
            ->select( "studentNum", "firstName", "lastName", "DOB", "address", "gender", "phoneNum", "email" )
            ->join('users', 'users.id', '=', 'studentInfos.userId')
            ->get();

        $allstudents = array_map(function($object){
            return (array) $object;
        }, $allstudents);

        return view('users.admin.crewManagement.students.view',
            ['students'=> $allstudents]);
    }

    /**
     * This method lists out all the teachers' records.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewTeachers(){
        $allteachers = \DB::table('teacherInfos')
            ->select( "teacherNum", "firstName", "lastName", "DOB", "gender", "phoneNum", "email", "language")
            ->join('users', 'users.id', '=', 'teacherInfos.userId')
            ->get();

        $allteachers = array_map(function($object){
            return (array) $object;
        }, $allteachers);

        return view('users.admin.crewManagement.teachers.view',
            ['teachers'=> $allteachers]);
    }

    /**
     * This method lists out all the instruments' records.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewInstruments(){
        $instruments = \DB::table('instruments')
            ->get();

        $instruments = array_map(function($object){
            return (array) $object;
        }, $instruments);

        return view('users.admin.instrumentManagement',
            ['instruments'=>$instruments]);
    }

    /**
     * This method lists out all the enrollments' records.
     *
     * @return \Illuminate\Http\Response
     */
    public function enrollmentArrangement(){
        $records = \DB::table('enrollment')
                ->select( "skills.skill", "enrollment.id", "enrollment.studentId", "enrollment.teacherId", "startDate", "endDate", "time", "weekday", "tuitionFee" )
                ->join('skills','skills.id','=','enrollment.skillId')
                ->get();

        $records = array_map(function($object){
            return (array) $object;
        }, $records);

        return view('users.admin.enrollmentArrangement',
            ['enrollments'=>$records]);
    }

    /**
     * This method adds new instrument into database.
     *
     * @return \Illuminate\Http\Response
     */
    public function addNewInstruments(Request $request){
        try {
            $data = $request->only("category","cost");

//            if(is_double($data["cost"])){
                \DB::table('instruments')->insert(
                    ['category' => $data["category"], 'cost' => $data["cost"]]
                );
                return redirect('/admin/instrumentManagement');
//            }else{
//                return back()->withInput()->withErrors("Cost must be a number");
//            }

        }catch (Exception $e){
            return back()->withInput()->withErrors("Some errors, please try it again");
        }
    }

}
