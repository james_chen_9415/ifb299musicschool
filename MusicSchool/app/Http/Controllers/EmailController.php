<?php

namespace App\Http\Controllers;

use Mail;

use Illuminate\Http\Request;

use App\Http\Requests;

/**
 *  The EmailController class performs all the backend activities related to sending email.
 */
class EmailController extends Controller
{
    /**
     * This method sends email to the specific email account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function send(Request $request)
    {
        $title = $request->input('title');
        $content = $request->input('content');

        Mail::send('email.send', ['title' => $title, 'content' => $content], function ($message)
        {

            $message->from('qut.toni.9516778@gmail.com', 'IFB299');

            $message->to('qut.toni.9516778@gmail.com');

        });

        return response()->json(['message' => 'Request completed']);
    }
}
