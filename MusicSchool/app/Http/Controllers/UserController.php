<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\User;
use App\Enrollment;
use App\EnrollmentFeedback;
use App\StudentInfo;
use App\TeacherInfo;
use App\Admin;
use App\Owner;
use Carbon\Carbon;

/**
 *  The UserController class performs all the backend activities related to user accounts.
 */
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Not planned to implement
    }

    /**
     * This method return the view that shows the form of register as a new user.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * This method store the new user into database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, User::$create_validation_rules);

        // Create user login
        $data = $request->only('role', 'email', 'password', 'password_confirmation');
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);


        // Use another table to store the user's profile information
        if ($user->role == 'teacher') {
            $role = new TeacherInfo;
        } else if($user->role == 'student') {
            $role = new StudentInfo;
            $role->parentsId = 1;
        }
        $role->lastName = $request->lastName;
        $role->firstName = $request->firstName;
        $role->userId = $user->id;

        $role->save();

        if ($user) {
            \Auth::login($user);
            return redirect('/profile/edit');
        }

        return back()->withInput();

    }

    /**
     * This method displays the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Not planned to implement
    }

    /**
     * This method redirect the edit controll to the relavant view in order to show the edit form.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->role == "student") {
            return view('users.student.profileEdit');
        } else if(Auth::user()->role == "teacher"){
            return view('users.teacher.profileEdit');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->role == "student") {
            $updateRec = StudentInfo::findOrFail($id);

            // Handle form input
            $input = $request->only('firstName', 'lastName','gender', 'phoneNum', 'address', 'DOB', 'FacebookId');
            $updateRec->firstName = $input['firstName'];
            $updateRec->lastName = $input['lastName'];
            $updateRec->phoneNum = $input['phoneNum'];
            $updateRec->DOB = $input['DOB'];
            $updateRec->address = $input['address'];
            $updateRec->FacebookId = $input['FacebookId'];

            // determine the relavant code for gender
            if($input['gender'] == "Male" || $input['gender'] == 1){
                $updateRec->gender = 1;
            }elseif($input['gender'] == "Female" || $input['gender'] == 2){
                $updateRec->gender = 2;
            }else{
                $updateRec->gender = 0;
            }

            // commit the change to the database
            $updateRec->save();

            // return the view to user
            return view('users.student.profile',
                            ['model' => $updateRec]
                        );
        } else if (Auth::user()->role == "teacher") {
            $updateRec = TeacherInfo::findOrFail($id);

            // Handle form input
            $input = $request->only('firstName', 'lastName','gender', 'phoneNum', 'address', 'DOB', 'FacebookId','skill', 'language' );
            $updateRec->firstName = $input['firstName'];
            $updateRec->lastName = $input['lastName'];
            $updateRec->phoneNum = $input['phoneNum'];
            $updateRec->DOB = $input['DOB'];
            $updateRec->address = $input['address'];
            $updateRec->FacebookId = $input['FacebookId'];
            $updateRec->language = $input['language'];
            //$updateRec->skills = $input['skill'];

            // determine the relavant code for gender
            if($input['gender'] == "Male" || $input['gender'] == 1){
                $updateRec->gender = 1;
            }elseif($input['gender'] == "Female" || $input['gender'] == 2){
                $updateRec->gender = 2;
            }else{
                $updateRec->gender = 0;
            }

            // commit the change to the database
            $updateRec->save();

            // reload the user info
            $model = TeacherInfo::where('userId', Auth::user()->id)->first();

            // return the view to user
            return view('users.teacher.profile',
                            ['model' => $model]
                        );
        }
    }

    /**
     * This method removes the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Not planned to implement
    }
    
    /**
     * This method lists out all the lesson enrolled by the user.
     *
     * @param  int  $userId
     * @return list of leasson records
     */
    private function getEnrollmentLesson($userId){
        // find out the student number by the user id
        $student = \DB::table('studentInfos')
            ->select( "studentNum" )
            ->join('users', 'users.id', '=', 'studentInfos.userId')
            ->where('id','=', $userId)
            ->get();

        // load all the related lesson records
        $allEnrollment = \DB::table('enrollment')
            ->select( "teacherInfos.firstName", "skills.skill", "startDate", "endDate", "time", "weekday", "tuitionFee" )
            ->join('teacherInfos', 'teacherInfos.teacherNum', '=', 'enrollment.teacherId')
            ->join('skills','skills.id','=','enrollment.skillId')
            ->where('studentId','=',$student[0]->studentNum)
            ->where('endDate','>',Carbon::now()->format('Y-m-d'))
            ->get();

        return $allEnrollment;
    }

    /**
     * This method lists call the home blade depends on the user type.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(){
        if (Auth::user()->role == "student") {
            /* Student home page */
            $model = StudentInfo::where('userId', Auth::user()->id)->get()->first();
            $enrollments= $this::getEnrollmentLesson(Auth::user()->id) ;

            return view('users.student.home',
                ['model' => $model, 'enrollments'=> $enrollments]
            );
        } else if (Auth::user()->role == "teacher") {
            /* Teacher home page */
            $model = TeacherInfo::where('userId', Auth::user()->id)->get()->first();
            return view('users.teacher.home',
                ['model' => $model]
            );
        } else if (Auth::user()->role == "admin") {
            /* Admin home page */
            $model = Admin::where('userId', Auth::user()->id)->get()->first();
            return view('users.admin.home',
                ['model' => $model]);
        } else if (Auth::user()->role == "owner") {
            /* Owner home page */
            return view('users.owner.home');
        } else {
            /* File not found error page */
            return abort(404);
        }
    }
    
    /**
     * This method gets all the histoy marks that the student enrolled and completed.
     *
     * @return \Illuminate\Http\Response
     */
    public function historyMarks(){
        if (Auth::user()->role == "student") {
            $studentNo = StudentInfo::where('userId', Auth::user()->id)->value("studentNum");

            $allhistory = \DB::table('enrollment')
                ->select( "teacherInfos.firstName", "skills.skill", "enrollment.id", "enrollment.studentId", "startDate", "endDate", "time", "weekday", "tuitionFee","mark" )
                ->join('teacherInfos', 'teacherInfos.teacherNum', '=', 'enrollment.teacherId')
                ->join('skills','skills.id','=','enrollment.skillId')
                ->where('studentId','=',$studentNo)
                ->where('endDate','<',Carbon::now()->format('Y-m-d'))
                ->get();

            /* pass the data to the view */
            return view('users.student.historyMarks',
                            ['model' => $allhistory]
                        );
        } else {
            /* File not found error page */
            return abort(404);
        }
    }

    /**
     * This method prepare the data needed for the apply lessons page for teacher.
     *
     * @return \Illuminate\Http\Response
     */
    public function applyLessons(){
        if (Auth::user()->role == "student") {
            /* Students are not permitted to view this website */
            echo "Students have no permissions to view the website (teacher apply lessons)";
            sleep(1);
            return back()->withInput();
        } else if (Auth::user()->role == "teacher") {
            /* teacher role is allowed to view the apply lesson form */
            $model = TeacherInfo::where('userId', Auth::user()->id)->first();
            return view('users.teacher.applyLessons',
                            ['model' => $model]
                        );
        }
    }

    /**
     * This method receive the apply lesson's form data and save it to database.
     *
     * @return \Illuminate\Http\Response
     */
    public function applyHandle(Request $request, $id)
    {
        if (Auth::user()->role == "student") {
            /* Denail student from accessing this page */
            echo "Students have no permissions to view the website (teacher apply lessons)";
            return back()->withInput();
        } else if (Auth::user()->role == "teacher") {
            $updateRec = TeacherInfo::findOrFail($id);

            // keep the data as it is
            $input = $request->only('skill');
            $updateRec->firstName =$updateRec->firstName;
            $updateRec->lastName = $updateRec->lastName;
            $updateRec->phoneNum = $updateRec->phoneNum;
            $updateRec->DOB =  $updateRec->DOB;
            $updateRec->address =$updateRec->address;
            $updateRec->FacebookId =$updateRec->FacebookId;
            $updateRec->language = $updateRec->language;
            $updateRec->gender =$updateRec->gender;

            // receive form data
            $updateRec->skills = $input['skill'];
            $updateRec->isHired = 0;

            // commit the changes
            $updateRec->save();

            // return to the view
            return redirect()->route('profile');
        }
    }

    /**
     * This method shows the profile data.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(){
        if (Auth::user()->role == "student") {
            $model = StudentInfo::where('userId', Auth::user()->id)->first();
            return view('users.student.profile',
                            ['model' => $model]
                        );
        } else if (Auth::user()->role == "teacher") {
            $model = TeacherInfo::where('userId', Auth::user()->id)->first();
            return view('users.teacher.profile',
                            ['model' => $model]
                        );
        } else if (Auth::user()->role == "admin") {
            return view('users.admin.profile');
        } else if (Auth::user()->role == "owner") {
            return view('users.owner.profile');
        } else {
            return abort(404);
        }
    }

    /**
     * This method shows the profile data.
     *
     * @return \Illuminate\Http\Response
     */
    public function profileEdit(){
        if (Auth::user()->role == "student") {

            $model = StudentInfo::where('userId', Auth::user()->id)->first();
            $newStudent = 'false';
            if ($model->newStudent == 'Y') {
                $newStudent = 'true';
            }
            return view('users.student.profileEdit',
                ['model' => $model,
                    'newStudent' => $newStudent]
            );
        } else if (Auth::user()->role == "teacher") {
            $model = TeacherInfo::where('userId', Auth::user()->id)->first();
            if($model->phoneNum == 0) $model->phoneNum = "";
            return view('users.teacher.profileEdit', ['model'=>$model]);
        } else if (Auth::user()->role == "admin") {
            return view('users.admin.profileEdit');
        } else if (Auth::user()->role == "owner") {
            return view('users.owner.profileEdit');
        } else {
            return abort(404);
        }
    }

    /**
     * This method receives new profile data and update the database.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProfileUpdate(Request $request){
        $data = $request;
        $updateRec = StudentInfo::find($request->studentNum);

        // update the record by form data
        $updateRec->firstName = $data->firstName;
        $updateRec->lastName = $data->lastName;
        $updateRec->gender = $data->gender;
        $updateRec->phoneNum = $data->phoneNum;
        $updateRec->DOB = $data->DOB;
        $updateRec->address = $data->address;
        $updateRec->isNew = $data->isNew;
        $updateRec->FacebookId = $data->FacebookId;

        // commit the changes
        $updateRec->save();

        //  Check if the student age under 18 
        $age = (time()-strtotime($data->DOB))/(60*60*24*365);
        if (($age < 18) && ($updateRec->parentsId == 1)) {
            // if student is under 18 and the parent information is not provided yet
            // then the student need one more step to get parent's consent to complete the update process.
            return redirect()->route('editParentConsent');
        } else {
            return redirect()->intended('profile');
        }
    }

    /**
     * This method receives new profile data and update the database.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleFeedback(Request $request){
        $input = $request;

        // create a new record
        $feedback = new EnrollmentFeedback;

        // update the record by form data
        $feedback->enrollmentId = $input->classid;
        $feedback->studentId = $input->studentid;
        $feedback->subject = $input->subject;
        $feedback->feedback = $input->feedback;

        // commit the changes
        $feedback->save();

        // return a json response
        return response()->json((['status' => 'success']));
    }
}
