<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;

/**
 *  The CareerController class performs all the backend activities related to career.
 */
class CareerController extends Controller
{
    /**
     * This method redirect the request to the static view.
     *
     * @return \Illuminate\Http\Response
     */
    public function recruit(){
        return view('career.recruit');
    }

}
