<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $login_validation_rules = [
        'email' => 'required|email|exists:users',
        'password'=>'required'
    ];

    public static $create_validation_rules=[
        'firstName' => 'required|alpha',
        'lastName' => 'required|alpha',
        'role' => 'required',
        'email'=> 'required|email|unique:users',
        'password'=> 'required|min:6|confirmed',
        'password_confirmation'=> 'required|min:6'
    ];

}
