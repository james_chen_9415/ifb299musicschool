<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentInfo extends Model
{
    protected $table = 'studentInfos';
    public $primaryKey = 'studentNum';
    public $timestamps = false;

}
