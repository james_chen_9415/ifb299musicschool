<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherInfo extends Model
{
    protected $table = 'teacherInfos';
    public $primaryKey = 'teacherNum';
    public $timestamps = false;
}
