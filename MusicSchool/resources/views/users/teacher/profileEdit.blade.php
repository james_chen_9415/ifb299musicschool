@extends('layouts.master')

@section('content')

    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="home">Welcome,{{$model->firstName}}</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="/profile">My Profile</a></li>

			  <li class="inactive"><a href="/lesson/apply">Apply Lessons</a></li>
			  	        <li class="" ><a href="/teacher/mailBox">
	        <span class="glyphicon glyphicon-envelope"></span><span class="badge" style="background-color:red;">5</span></a>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

<div class="container">

	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Basic Information</h3>
	    <div class="nav navbar-nav navbar-right"></div>
	  </div>

	  <div class="panel-body" style="max-width:650px;margin-left: auto; margin-right: auto;">


		{!!Form::open(array('route'=> array('profile.edit.update', $model->teacherNum)))!!}
		{!! method_field('patch') !!}
		<div class="form-group">
			{!!Form::label('teacherNum', 'Teacher Number:')!!}
			{!!Form::text('teacherNum', $model->teacherNum, array('class'=> 'form-control', 'disabled'))!!}
		</div>
		<div class="form-group">
			{!!Form::label('email', 'Email:')!!}
			{!!Form::text('email', Auth::user()->email, array('class'=> 'form-control', 'disabled'))!!}
		</div>
		<div class="form-group">
			{!! Form::label('firstName', 'First Name:') !!}
			{!! Form::text('firstName', $model->firstName, array('class'=> 'form-control')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('lastName', 'Last Name:') !!}
			{!! Form::text('lastName', $model->lastName , array('class'=> 'form-control')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('gender', 'Gender:') !!}
            @if($model->gender == 0)
				{!!Form::radio('gender', '1')!!} Male
				{!!Form::radio('gender', '2')!!} Female
            @elseif($model->gender == 1)
                {!! Form::text('gender', "Male" , array('class'=> 'form-control','readonly')) !!}
			@elseif($model->gender == 2)
				{!! Form::text('gender', "Female" , array('class'=> 'form-control','readonly')) !!}
            @endif
        </div>
		<div class="form-group">
			{!! Form::label('phoneNum', 'Phone Number:') !!}
			{!! Form::text('phoneNum',$model->phoneNum , array('class'=> 'form-control')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('DOB', 'Date of Birth:') !!}
			{!! Form::date('DOB', \Carbon\Carbon::parse( $model->DOB)) !!}
		</div>
		<div class="form-group">
			{!! Form::label('address', 'Address:') !!}
			{!! Form::text('address', $model->address , array('class'=> 'form-control')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('language', 'Langeuage Skills:') !!}
			{!! Form::text('language', $model->language , array('class'=> 'form-control')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('FacebookId', 'Facebook ID:') !!}
			{!! Form::text('FacebookId', $model->FacebookId , array('class'=> 'form-control')) !!}
		</div>
		{!! Form::token() !!}
		{!! Form::submit('Submit', array('class'=> 'btn btn-default')) !!}
		{!! Form::close() !!}
		</div>
</div>
@endsection