@extends('layouts.master')

@section('content')

    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="home">Welcome, {{ $model->firstName }}</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
			  <li class="inactive"><a href="/profile">My Profile</a></li>
			  <li class="inactive"><a href="/lesson/apply">Apply Lessons</a></li>
			  	        <li class="" ><a href="/teacher/mailBox">
	        <span class="glyphicon glyphicon-envelope"></span><span class="badge" style="background-color:red;">5</span></a>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

<div class="container">
	<div class="col-md-12 col-lg-6" style="border:1px solid #999; border-radius:5px">
		<h3 class="table-caption">Enrolled Classes</h3>
		<table class="table table-striped">
			<tr>
				<th>Lesson</th>
				<th>Date</th>
				<th>Duration</th>
			</tr>
			<tr>
				<td>Classical</td>
				<td>Tue 13:00</td>
				<td>30 min</td>
			</tr>
			<tr>
				<td>Piano</td>
				<td>Fri 15:00</td>
				<td>30 min</td>
			</tr>
		</table>
	</div>
</div>
@endsection