@extends('layouts.master')

@section('content')
<div class="container">

<div class="panel panel-default" style="width:80%;margin-left: auto; margin-right: auto;">
    <div class="panel-heading"><h2>Register</h2></div>

    <div class="panel-body" style="max-width:600px; margin-left: auto; margin-right: auto;">
    @if(count($errors))

        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="alert alert-info" role="alert">* All fields are required.</div>

    {!!Form::open(array('route'=> 'users.store'))!!}
        <div class="form-group">
            I want to be a 
            {!!Form::radio('role', 'student')!!} student
            {!!Form::radio('role', 'teacher')!!} teacher
        </div>

        <div class="form-group">
            {!!Form::label('firstName', 'First Name')!!}
            {!!Form::text('firstName', null, array('class'=> 'form-control'))!!}
        </div>
        <div class="form-group ">
            {!!Form::label('lastName', 'Last Name')!!}
            {!!Form::text('lastName', null, array('class'=> 'form-control'))!!}
        </div>
        <div class="form-group ">
            {!! Form::label('email', 'Email') !!}
            {!! Form::text('email', null, array('class'=> 'form-control')) !!}
        </div>
        <div class="form-group ">
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', array('class'=> 'form-control')) !!}
        </div>
        <div class="form-group ">
            {!! Form::label('password_confirmation', 'Confirm Password') !!}
            {!! Form::password('password_confirmation', array('class'=> 'form-control')) !!}
        </div>
    {!! Form::token() !!}
    {!! Form::submit('Register', array('class'=> 'btn btn-default', 'value'=>'Register')) !!}
    {!! Form::close() !!}
    </div>
    </div>
   
</div>
@endsection