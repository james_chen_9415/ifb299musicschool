@extends('layouts.master')

@section('content')

    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	
			<a class="navbar-brand" href="/admin">Welcome, Admin1</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">

	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

<div class="container-fluid">

		<!--Sidebar tools -->
	<div class="row">
		<div class="col-sm-3 ">
			      <ul class="nav nav-pills nav-stacked">
        <li><a href="/admin">Home</a></li>
        <li><a href="/admin/lessonArrangement">Lessons</a></li>
        <li><a href="/admin/enrollmentArrangement">Enrolment</a></li>
        <li>
        <a class="" data-toggle="collapse" href="#collapse1">Members
        <span class="caret"></span></a>
        <div id="collapse1" class="panel-collapse collapse">
        <ul class="list-group">
          <li class="list-group-item"><a href="/admin/crewManagement/students/view">Student</a></li>
          <li class="list-group-item"><a href="/admin/crewManagement/teachers/view">Teacher</a></li>
        </ul>
        </div>
        </li>
        <li  class="active"><a href="/admin/instrumentManagement">Managing Instrument</a></li>
      </ul><br>
		</div>

		<!--End of sidebar -->
	<div class="col-sm-9">
	<!-- filter field -->
	<div>
		  <form>
    <div class="form-group">
    <div>
    	 <a type="button" class="btn btn-primary" data-toggle="collapse" href="#collapse2">Filter</a>
    	 <div id="collapse2" class="panel-collapse collapse">

    	     <label for="sel1">Choose Instrument Type:
</label>
      <select class="" id="sel1">
              <option>All</option>
        <option>Guitar</option>
        <option>Cello</option>
      </select>
    	   
    	   <label for="sel1"> Choose Condition:</label>
      <select class="" id="sel1">
              <option>All</option>
        <option>New</option>
        <option>Exellent</option>
        <option>Good</option>
 		<option>Repair</option>
 		<option>Discard</option>

      </select>
      <br>
    	    Choose Status: 
    	    <label><input type="radio" name="optradio">Default</label>
    	    <label><input type="radio" name="optradio">Available</label>
      	    <label><input type="radio" name="optradio">Hired</label>

      	     <br>
       	       <label for="sel1">Order by</label>
      <select class="" id="sel1">
        <option>ID</option>
        <option>Cost</option>

      </select>
 <br>

       <button type="submit" class="btn btn-default">Go</button>
    	 </div>

    </div>

</form>
</div><!--end of filter-->


        @if(count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h2>Instrument List</h2><hr>
	 <table class="table table-list-search">
                    <thead>
                        <tr>
                            <th><i>Instrument Id</i></th>
                            <th><i>Image</i></th>
                            <th><i>Type</i></th>
                            <th><i>Information</i></th>
                            <th><i>Hire cost</i></th>
                            <th><i>Condition</i></th>
                            <th><i>Status</i></th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($instruments as $instrument)
                        <tr>
                            <td>{{$instrument["id"]}}</td>
                            <td><image src="/resources/images/yamaha.png" class="thumbnail" width="80" height="80"></td>
                            <td>{{$instrument["category"]}}</td>
                            <td><i>{{$instrument["info"]}}</td>
                            <td>{{$instrument["cost"]}}</td>
                            <td>New</td>
                            <td>
                                @if($instrument["hire_date"]!=NULL)
                                    <a style="height: 25px" class="label label-info">Hired</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
 

                           <tr id="collapse3" class="panel-collapse collapse">
                           <form method="post" action="/admin/instrumentManagement">
                               <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                <td>id+1</td>
                               <td><button type="button" class="btn btn-primary">Select Image</button></td>
                                <td>
                                <select class="" id="sel1" name="category">
                                    <option>Guitar</option>
                                    <option>Cello</option>
                                    <option>Piano</option>
                                    <option>Drums</option>
                                </select></td>
                                <td>$<input type="" value="Input cost" name="cost"></td> <!-- has to be valid -->
                                <td>
                                <select class="" id="sel1" name="condition">
                                    <option>New</option>
                                    <option>Exellent</option>
                                    <option>Good</option>
                                    <option>Repair</option>
                                    <option>Discard</option>

                                </select></td>
                            <!-- statu has to be automatly as available for new added instrument-->
                            <td><input type="submit" name="Submit"></td>
                         </form>
                        </tr>


 
                         </tbody>
                </table>   
				<button type="button" class="btn btn-primary">Edit</button>

                <a type="button" class="btn btn-primary" data-toggle="collapse" href="#collapse3">Add new</a>

	</div><!--end of content-->



</div>
@endsection