@extends('layouts.master')

@section('content')

    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      {{--<a class="navbar-brand" href="home">Welcome, {{ $model->firstName }}</a>--}}
			
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
			  <li class="inactive"><a href="/profile">My Profile</a></li>
			  <li class="inactive"><a href="/lesson/apply">Apply Lessons</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

<div class="container-fluid">

		<!--Sidebar tools -->
    <div class="row">
        <div class="col-sm-3 ">
                  <ul class="nav nav-pills nav-stacked">
              <li><a href="/admin">Home</a></li>
        <li><a href="/admin/lessonArrangement">Lessons</a></li>
        <li><a href="/admin/enrollmentArrangement">Enrolment</a></li>
        <li class="active">
        <a class="" data-toggle="collapse" href="#collapse1">Members
        <span class="caret"></span></a>
        <div id="collapse1" class="panel-collapse collapse">
        <ul class="list-group">
          <li class="list-group-item"><a href="/admin/crewManagement/students/view">Student</a></li>
          <li class="list-group-item"><a href="/admin/crewManagement/teachers/view">Teacher</a></li>
        </ul>
        </div>
        </li>
        <li><a href="/admin/instrumentManagement">Managing Instrument</a></li>
      </ul><br>
        </div>

        <!--end of side bar -->


	<div class="col-sm-9">

	<div>
		  <form>
    <div class="form-group">
    <div>
    	 <a type="button" class="btn btn-primary" data-toggle="collapse" href="#collapse2">Filter</a>
    	 <div id="collapse2" class="panel-collapse collapse">

    	    <label class="checkbox-inline">
               <input type="checkbox" value="">Show only under 18
            </label>
            <br>

            Choose Status:
                    <label class="checkbox-inline">
               <input type="checkbox" value="">Enroled</label>
               <input type="checkbox" value="">Papare to enrol</label>
               <input type="checkbox" value="">Graduated</label>
               <br>
                Choose Gender: 
            <label><input type="radio" name="optradio">All</label>
            <label><input type="radio" name="optradio">Female</label>
            <label><input type="radio" name="optradio">Male</label>
            <br>
       	       <label for="sel1">Order by</label>
      <select class="" id="sel1">
        <option>ID</option>
        <option>Name</option>
        <option>Age</option>
      </select>

       <button type="submit" class="btn btn-default">Go</button>
    	 </div>

    </div>


    
</form>
</div><!--end of filter-->
<h2>Student List</h2><hr>
	 <table class="table table-list-search">
                    <thead>
                        <tr>
                            <th><i>Student Id</i></th>
                            <th><i>First Name</i></th>
                            <th><i>Last Name</i></th>
                            <th><i>DOB</i></th>
                            <th><i>Address</i></th>
                            <th><i>Gender</i></th>
                            <th><i>Phone Number</i></th>
                            <th><i>Email</i></th>
                            <th><i>Status</i></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($students as $student)
                        <tr>
                            <td>{{$student['studentNum']}}</td>
                            <td>{{$student['firstName']}}</td>
                            <td>{{$student['lastName']}}</td>
                            <td>{{$student['DOB']}}</td>
                            <td>{{$student['address']}}</td>
                            @if($student['gender'] == 1)
                                <td>Male</td>
                            @else
                                <td>Female</td>
                            @endif
                            <td>{{$student['phoneNum']}}</td>
                            <td>{{$student['email']}}</td>
                        <td><a style="height: 25px" class="label label-info">Enroled</a></td>
                        </tr>
                        @endforeach
                           <tr id="collapse3" class="panel-collapse collapse"> <!--start of add new meue-->
                           <form method="post" action="/admin/instrumentManagement">
                               <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                <td>id+1</td>
                                <td><input type="" value="First Name" name=""></td>
                                <td><input type="" value="Last Name" name=""></td>
                                <td><input type="" value="DoB" name=""></td><!-- calendar selector -->
                                <td><input type="" value="Address" name=""></td>
                               
                                <td><select class="" id="sel1" name="">
                                    <option>Male</option>
                                    <option>Female</option>
                                </select></td>
                           <td><input type="" value="PhoneNumber" name=""></td>
                           <td><input type="" value="Email Address" name=""></td>
                            <td><input type="submit" name="Submit"></td>
                         </form>
                        </tr><!-- end of add new -->
 
                         </tbody>
                </table>   
                <button type="button" class="btn btn-primary">Edit</button>
                <a type="button" class="btn btn-primary" data-toggle="collapse" href="#collapse3">Add new</a>

	</div><!--end of content-->



</div>
@endsection