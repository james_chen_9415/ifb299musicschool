@extends('layouts.master')

@section('content')

    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      {{--<a class="navbar-brand" href="home">Welcome, {{ $model->firstName }}</a>--}}
			<a class="navbar-brand" href="home">Welcome, Renzo</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
			  <li class="inactive"><a href="/profile">My Profile</a></li>
			  <li class="inactive"><a href="/lesson/apply">Apply Lessons</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

<div class="container-fluid">
		<!--Sidebar tools -->
	<div class="row">
		<div class="col-sm-3 ">
			      <ul class="nav nav-pills nav-stacked">
         <li><a href="/admin">Home</a></li>
        <li class="active"><a href="/admin/lessonArrangement">Lessons</a></li>
        <li><a href="/admin/enrollmentArrangement">Enrolment</a></li>
        <li>
        <a class="" data-toggle="collapse" href="#collapse1">Members
        <span class="caret"></span></a>
        <div id="collapse1" class="panel-collapse collapse">
        <ul class="list-group">
          <li class="list-group-item"><a href="/admin/crewManagement/students/view">Student</a></li>
          <li class="list-group-item"><a href="/admin/crewManagement/teachers/view">Teacher</a></li>
        </ul>
        </div>
        </li>
        <li><a href="/admin/instrumentManagement">Managing Instrument</a></li>
      </ul><br>
		</div>

				<!-- Content -->
		<div class="col-sm-9">
		<h2>Manage Lessons</h2><hr>
			 <table class="table table-list-search">
                    <thead>
                        <tr>
                            <th><i>Teacher Id</i></th>
                            <th><i>Week Day</i></th>
                            <th><i>Room</i></th>
                            <th><i>Tuition Fee</i></th>
 
                        </tr>
                    </thead>
                    <tbody>

                      
                        <tr>
                           <td></td>
                           <td></td>
							<td></td>
							 <td></td>
                        </tr>
                      

 
                         </tbody>
                </table>  


		</div><!--end of content-->

</div> <!--end of container -->
@endsection