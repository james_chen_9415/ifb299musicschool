@extends('layouts.master')

@section('content')

    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="/home">Welcome, {{$model->firstName}}</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="/profile">My Profile</a></li>
	        <li class=""><a href="/student/historyMarks">Enrol History</a></li>

	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
<div class="container">
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Basic Information</h3>
	    
	    		<form method="get" action="/profile/edit">
	    				<input type="submit" value="Edit" class="btn btn-primary" />
	    		</form>

	    
	  </div>
	  <div class="panel-body">
	    <table>
	    	<tr>
	    		<td>Student Number:</td>
	    		<td>{{$model->studentNum}}</td>
	    	</tr>
	    	<tr>
	    		<td>Email:</td>
	    		<td>{{Auth::user()->email}}</td>
	    	</tr>
	    	<tr>
	    		<td>First Name:</td>
	    		<td>{{$model->firstName}}</td>
	    	</tr>
	    	<tr>
	    		<td>Last Name:</td>
	    		<td>{{$model->lastName}}</td>
	    	</tr>
	    	<tr>
	    		<td>Gender:</td>
	    		<td>
	    			@if ($model->gender == 1)
	    			M
	    			@else
	    			F
	    			@endif
	    		</td>
	    	</tr>
	    	<tr>
	    		<td>Phone Number:</td>
	    		<td>{{$model->phoneNum}}</td>
	    	</tr>
	    	<tr>
	    		<td>Date of Birth:</td>
	    		<td>{{$model->DOB}}</td>
	    	</tr>
	    	<tr>
	    		<td>Address:</td>
	    		<td>{{$model->address}}</td>
	    	</tr>
	    	<tr>
	    		<td>New Student:</td>
	    		<td>{{$model->isNew}}</td>
	    	</tr>
	    	<tr>
	    		<td>Facebook ID:</td>
	    		<td>{{$model->FacebookId}}</td>
	    	</tr>
	    </table>
	  </div>
	</div>
</div>
@endsection