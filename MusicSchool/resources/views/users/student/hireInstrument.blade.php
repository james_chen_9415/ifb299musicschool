@extends('layouts.master')

@section('content')
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <div class="navbar-brand">Hire Instrument</div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class=""><a href="/profile">My Profile</a></li>
            <li class=""><a href="/student/historyMarks">Enrol History</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

  <div class="container">
  <div id="hire-application" class="panel panel-default">
   <div class="panel-heading">
      
    <h3 class="table-caption">Choose your instrument</h3>
    </div>
              <form method="get" action="">
    
          
<select id="instrument-filter">
  <option value="All">All</option>
  <option value="Guitar">Guitar</option>
  <option value="Cello">Cello</option>
  <option value="Piano">Piano</option>
  <option value="Percussion">Percussion</option>
</select>
<select id="orderby">
  <option value="" selected>Order by</option>
  <option value="instrument">Instrument</option>
  <option value="cost">Cost</option>
 
</select>

    <div id="loading" style="display:none;" class="alert alert-info text-center">Loading...</div>
    <table id="instrument-list" class="table table-striped">
      <tr>
        <th>Instrument</th>
         <th>ID</th>
        <th>info</th>
        <th>Cost per month</th>
             <td> Select</td>
       
      </tr>
    </table>    
    <div id="form-submit">
      <input id="submit-hire" type="submit" value="Hire instrument" class="btn btn-primary" />
    </div>
    </form>
  </div>
  <div id="hire-success" style="display:none;">
      <p class="alert alert-success" role="alert">Your booking is being process, please pay the fee at the reception and get your instrument within 3 days.</p>
      <p><button id="btn-reloadform" class="btn btn-primary">Hire more instrument</button></p>
    </div>
  </div>

  <script language="javascript">
  var data;
  var headerRow = "<tr><th>Instrument</th><th>ID</th><th>info</th><th>Cost per month</th><td> Select</td></tr>";

  function load_list() {
    $("#loading").slideToggle();
    $("#instrument-list").toggle();
    try{
        $.ajax({
          type: 'get',
          url: '/student/query-instrumentlist',
          data: {
            'orderby': $("#orderby option:selected").val()
          },
          success: function(response, status, xhr) {
            try{
              data = $.parseJSON(xhr.responseText);
              var rowNum = 0;
              $("#instrument-list").empty();
              $("#instrument-list").append(headerRow);
              while(data[rowNum] != null){
                $("#instrument-list").append("<tr><td>"+
                                            data[rowNum].category+"</td><th>#"+
                                            data[rowNum].id+"</th><td>"+
                                            data[rowNum].info+"</td><td>"+
                                            data[rowNum].cost+"</td><th>"+
                                            "<input type=\"radio\" name=\"hireid\" value=\""+data[rowNum].id+"\" > <br></th><</tr>");
                rowNum++;
              }
            } catch(e){
              console.log("Error on ajax success.\n"+e.message);
            }
          $("#loading").slideToggle();
          $("#instrument-list").toggle();
          },
          error: function(response, status, xhr){
            alert("There is an error on submitting the enquiry, please try it again!");
          }         
        }); 
      } catch(e){
        console.log("Error on ajax process.\n"+e.message);
      }
    }

    load_list();

    $("#instrument-filter").on("change",function(){
      var keyword = $("#instrument-filter option:selected").val();
      $("#instrument-list").empty();
      $("#instrument-list").append(headerRow);
      var rowNum = 0;
      while(data[rowNum] != null){
        if((data[rowNum].category == keyword) || (keyword == "All")) {
          $("#instrument-list").append("<tr><td>"+
                                          data[rowNum].category+"</td><th>#"+
                                          data[rowNum].id+"</th><td>"+
                                          data[rowNum].info+"</td><td>"+
                                          data[rowNum].cost+"</td><th>"+
                                          "<input type=\"radio\" name=\"hireid\" value=\""+data[rowNum].id+"\" > <br></th></tr>");
        }
        rowNum++;
      }
    });

    $("#orderby").on("change", function(){
      load_list();
    });

    $("#submit-hire").on("click", function(e){
      e.preventDefault();
      try{
        $("#submit-hire").prop('disabled', true);
        $.ajax({
          type: 'get',
          url: '/student/query-hireInstrument',
          data: {
            'hireid': $("input[name='hireid']:checked").val(),
            'user' : '{{Auth::user()->id}}'
          },
          success: function(response, status, xhr) {
            try{
              data = $.parseJSON(xhr.responseText);
              if (data.status == 'true') {
                $("#hire-application").hide();
                $("#hire-success").show();
              } else {
              alert("There is an error on submitting the enquiry, please try it again!");
              }
            } catch(e){
              console.log("Error on ajax success.\n"+e.message);
            }
          },
          error: function(response, status, xhr){
            alert("There is an error on submitting the enquiry, please try it again!");
          }         
        }); 
      } catch(e){
        console.log("Error on ajax process.\n"+e.message);
      }
        $("#submit-hire").prop('disabled', false);
    });

    $("#btn-reloadform").on("click", function(e){
      e.preventDefault();
      window.location.reload();
    });
  </script>
@endsection