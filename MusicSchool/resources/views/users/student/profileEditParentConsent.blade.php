@extends('layouts.master')

@section('content')

    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="/home">{{$firstName}}</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="/profile">My Profile</a></li>
	        <li class=""><a href="/student/historyMarks">Enrol History</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Parent Information</h3>
	  </div>
	  <div class="panel-body">
	  	@if ($alert)
	  	<div class="alert alert-warning" role="alert">Please provide your parent's information.</div>
	  	@endif
	  	{!! Form::open(array('route'=>'handleProfileUpdate')) !!}
	    <table>
	    	<tr>
	    		<td>Student Number:</td>
	    		<td>{{$id}}</td>
	    	</tr>
	    </table>
	    <div class="form-group">
            {!! Form::label('First Name') !!}
            {!! Form::text('firstName', null, array('class'=> 'form-control')) !!}
        </div>
	    <div class="form-group">
            {!! Form::label('Last Name') !!}
            {!! Form::text('lastName', null, array('class'=> 'form-control')) !!}
        </div>
	    <div class="form-group">
            {!! Form::label('Gender') !!}
            {!!Form::radio('gender', '1')!!} Male
	        {!!Form::radio('gender', '0')!!} Female
        </div>
	    <div class="form-group">
            {!! Form::label('Email') !!}
            {!! Form::text('email', null, array('class'=> 'form-control')) !!}
        </div>
	    <div class="form-group">
            {!! Form::label('phoneNum') !!}
            {!! Form::text('phoneNum', null, array('class'=> 'form-control')) !!}
        </div>
	    <div class="form-group">
            {!! Form::label('Address') !!}<br/>
            {!! Form::checkbox('sameAsStudent', 'same', false) !!} same as student
            {!! Form::text('address', null, array('class'=> 'form-control',
            			'data' => $address)) !!}
            <script>
            $("form input[name='sameAsStudent']").change(function(){
            	if ($(this).is(":checked")) {
            		$("form input[name=address]").val($("form input[name=address]").attr("data"));
            	}
            });
            </script>
        </div>
        <div class="form-group">
            {!! Form::submit('Save', array('class'=> 'form-control btn btn-primary')); !!}
        </div>
	  </div>
	</div>

@endsection