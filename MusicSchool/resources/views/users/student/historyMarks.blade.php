@extends('layouts.master')

@section('content')
    <script src="/resources/js/studentpage-writefeedback.js"></script>   

    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-brand">Enrolment History</div>
        </div>

<!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class=""><a href="/profile">My Profile</a></li>
	        <li class="active"><a href="/student/historyMarks">Enrol History</a></li>

	      </ul>
	    </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <div class="container" style="clear:both;">
        <h3 class="table-caption">Enrolment History</h3>
		<table class="table table-striped">
			<tr>
				<th>Lesson Instrument</th>
				<th>Date Commencement</th>
				<th>Date Ended</th>
				<th>Start Time</th>
				<th>Teacher</th>
				<th>Marks</th>
				<th>Message</th>
			</tr>

			@if ($model != null)
				@foreach($model as $record)
					@if ($record != null)
	             <tr>
					<td>{{$record->skill}}</td>
					<td>{{$record->startDate}}</td>
					<td>{{$record->endDate}}</td>
					<td>{{$record->time}}</td>
					<td>{{$record->firstName}}</td>
					<td>{{$record->mark}}</td>
					<td><button type="button" class="btn btn-info btn-sm write-feedback-btn"
						 data-toggle="modal" data-target="#myModal" data-from="{{$record->studentId}}" data-id="{{$record->id}}" 
						 >Write Feedback</button></td>
				</tr>
					@endif
            	@endforeach
            @endif
		</table>
		
		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<form id="feedback-form" action="/student/handleFeedback" method="post">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Write Feedback</h4>
				  </div>
				  <div class="modal-body">
					<div class="form-group">
						<label for="usr">Subject</label>
						<input type="text" class="form-control" id="usr" name="subject">
						<input type="hidden" class="form-control" id="student" name="student">
						<input type="hidden" class="form-control" id="class" name="class">
						<label for="comment">Your Feedback:</label>
						<textarea class="form-control" rows="5" id="feedback" name="feedback"></textarea>
					</div>
					
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" onClick="send_feedback()">Send</button>
				  </div>
				</form>
			</div>

		  </div>
		</div>

		<!-- Modal -->
		<div id="myModalResult" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<form id="feedback-form" action="/student/handleFeedback" method="post">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Write Feedback</h4>
				  </div>
				  <div class="modal-body">
				  	<p class="alert alert-success" role="alert">Your feedback has been saved.</p>
				  </div>
				</form>
			</div>

		  </div>
		</div>
		
		
    </div>
@endsection