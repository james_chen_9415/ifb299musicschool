@extends('layouts.master')

@section('content')

    <nav class="nav navbar-inverse">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">Welcome, {{ $model->firstName }}</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="/profile">My Profile</a></li>
	        <li class=""><a href="/student/historyMarks">Enrol History</a></li>

	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
<div class="container" style="clear:both;">
	
		<h3 class="table-caption">Enrolled Classes</h3>
		<table class="table table-striped">
			          <tr>
                            <th>Teacher</th><!-- It will be better if u can make this 2 columns display NAME insteaded of ID, but its optional -->
                            <th>Lesson Type</th>  <!-- Database doesnt have this column which I think is improtant-->
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Start time</th>
                            <th>WeekDay</th>
                            <th>Tuition Fee</th>
                            
                        </tr>
                   
                    <tbody>
                        @foreach($enrollments as $enrollment)
                       <tr>
                           <td>{{$enrollment->firstName}}</td>
                           <td>{{$enrollment->skill}}</td>
                           <td>{{$enrollment->startDate}}</td>
                           <td>{{$enrollment->endDate}}</td>
                           <td>{{$enrollment->time}}</td>
                           <td>{{$enrollment->weekday}}</td>
                            <td>{{$enrollment->tuitionFee}}</td>
                        </tr>
                       @endforeach
                      

 
                         </tbody>
		</table>

		<a class="btn btn-primary" href="/student/hireInstrument">Hire instrument</a>
	
</div>
@endsection