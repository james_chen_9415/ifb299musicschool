@extends('layouts.master')

@section('content')
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-brand" id="aboutustitle">About Us</div>
        </div>

	      </div><!-- /.container-fluid -->
    </nav>

    <div class="container" style="clear:both;">
      <div class="row">
	
	<div id="textaboutus">
		<h1>About us</h1><br/>
		<p>At Music School we strive to ensure you will achieve maximum improvement from your music lessons.</p>
		<p>We offer highly effective piano lessons, singing lessons and drum lessons in Brisbane for people of all ages and skill levels.</p>
		<p>We take the time to find out what it is you really need and want from your music lessons, help you to establish your goals and coach you towards achieving them.  Our highly skilled music teachers use specific techniques to help you realise your musical potential in lessons that are tailored to your interests and designed to help you achieve your musical ambitions in the shortest possible time.</p>
		<p>We are proud of the exceptional calibre of our staff.  Our music teachers are committed, passionate and expertly trained to bring the best possible results for you.</p>
		<p>If you are looking for professional, supportive and engaging music lessons on your instrument, we invite you to take the first step and contact us to discuss how we can help you in detail. Request your free music lesson today.</p>
		<p>We are a totally inclusive music teaching facility servicing Brisbane musicians of all ages, abilities and musical interests.</p>
		
		<p>Offering a diverse range of lessons including: </p>
		<ul>
		 
		<li>piano lessons</li>
		<li>voice lessons</li>
		<li>drum kit/percussion lessons</li>
		<li>guitar lessons</li>
		<li>theory classes</li>
		<li>coached band sessions</li>
		<li>performance opportunities</li>
		</ul>
	</div>
	
	<div id="jack">
		<img src="/resources/images/jack.jpg" alt="jack"/>
	</div>
	
	  </div>
    </div>
   
@endsection