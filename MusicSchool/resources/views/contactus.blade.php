@extends('layouts.master')

@section('content')
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-brand">Contact Us</div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="#">Link 1</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <div class="container" style="clear:both;">
      <div class="row">
        <div class="col-xs-12">
          <p><span style="font-weight:bold;">Address:</span><span>No. 34 Pinelands Road, Sunnkbank South, Brisbane, QLD 4109.</span></p>
          <p><span style="font-weight:bold;">Postal Address:</span><span>(same as above)</span></p>
          <p><span style="font-weight:bold;">Telphone No:</span><span> 07 3456 1234</span></p>
          <p><span style="font-weight:bold;">Email:</span><span>inqury@pinelandsmusic.com.au</span></p>
          <p><span style="font-weight:bold;">Opening Hours:</span><br/>
             <span>
                Monday - Friday: 9am - 5pm<br/>
                Satuaday: 9am - 2pm<br/>
             </span></p>
          <p><span style="font-weight:bold;"></span>
             <span>
              <a href="https://www.facebook.com/groups/523163221217593/?ref=ts&fref=ts"><img src="/resources/images/teaser/visit-facebook-icon.gif" border="0"/></a>
             </span></p>
        </div>
      </div>
    </div>
    <div style="display:block; height:350px; clear:both;">
      <div id="content-footer" class="row parallax-window" style="min-height: 350px; background: transparent;" data-parallax="scroll" data-image-src="/resources/images/student-bg.jpg">
      </div>
    </div>
@endsection