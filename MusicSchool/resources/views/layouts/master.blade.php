<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>
		@if(\Auth::check())
		Member Portal
		@else
		Pinelands Music School
		@endif
		</title>
		<Link rel="stylesheet prefetch" href="/resources/library/bootstrap-3.3.7-dist/css/bootstrap.css">
		<Link rel="stylesheet prefetch" href="/resources/css/frontend.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!--<script src="/resources/library/jquery/jquery-3.1.0.min.js" crossorigin="anonymous"></script>-->
		<script src="/resources/library/bootstrap-3.3.7-dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 
		<script src="/resources/library/parallax.js-1.4.2/parallax.min.js"></script>   
	</head>
	
	<body>
		
		<div id="top">

			<nav class="navbar navbar-default" style="height:100px; ">
				<div class="container-fluid">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
					<div class="navbar-header" id="logo">
						<a class="navbar-brand" href="/"><img src="/resources/images/logo.png"/></a>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="/" class="navbar-menu-links">Home</a></li>
						<li><a href="/lesson" class="navbar-menu-links">Lessons</a></li>
						<li><a href="/career" class="navbar-menu-links">Careers</a></li>
						<li class="dropdown">  <a class="dropdown-toggle " data-toggle="dropdown" href="#" id="navbar-menu-dropdown-link"><span class="glyphicon glyphicon-user"></span> My Account<span class="caret"></span></a>
							<ul class="dropdown-menu"><!--dropdown menu -->
								@if(\Auth::check())
								<li> {{ link_to_route('home', 'My Portal') }} </li>
								<li role="separator" class="divider"></li>
								<li>{{ link_to_route('logout', 'Logout') }} </li>
								@else
								<li>{{link_to_route('users.create','New User')}} </li>
								<li role="separator" class="divider"></li>
								<li>{{link_to_route('login','Login')}} </li>
								@endif
							</ul><!--end of dropdown menu-->
						</li>                
					</ul><!--end of navbar ul -->
					</div><!--end of collepse-->
				</div><!-- end of navbar-->
				
			</nav>
			
		</div><!--Close div top-->
            
		@yield('content')		
		
		
		<footer class="modal-footer ">
			<ul class="nav nav-pills">
				<li><a href="/contactus" class="footer-links" id="contactus">Contact Us</a>
				<li><a href="/aboutus" class="footer-links">About Us</a>
				<li><a href="" class="footer-links">Partnership</a>
				</li>
			</ul>
			
		</div>
		<span> <img src="/resources/images/icons.png" id="social-media-icons" ></span>
	</body>
</html>