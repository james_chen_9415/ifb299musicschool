@extends('layouts.master')

@section('content')
    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <div class="navbar-brand" href="#">Enrollment</div>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="#">Link 1</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>


<div class="container">
<div class="panel panel-default" style="max-width:800px;margin-left: auto; margin-right: auto;">
	<div class="panel-heading"><h2>Class Enrollment</h2></div>

	<div class="panel-body" style="max-width:650px;margin-left: auto; margin-right: auto;">
	@if(count($errors))
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
	@endif

		@if (\Session::has('success'))
			<div class="alert alert-success">
				<ul>
					<li>{!! \Session::get('success') !!}</li>
				</ul>
			</div>
		@endif

	{!! Form::open(array('route'=> 'lesson.enrollment.store')) !!}
	<div class="form-group">
		{!! Form::label('instrumentClass', 'Instrument Class:') !!}
		{!! Form::text('instrumentClass',$_GET['class'], array('class'=> 'form-control', 'readonly')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('teacher', 'Teacher:') !!}
		{!! Form::text('teacher', $_GET['teacherName'], array('class'=> 'form-control', 'readonly')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('startingDate','Starting Date:') !!}
		{!! Form::date('startingDate', \Carbon\Carbon::now()) !!}
	</div>
	<div class="form-group">
		{!! Form::label('endingDate','Ending Date:') !!}
		{!! Form::date('endingDate', \Carbon\Carbon::tomorrow()) !!}
	</div>
	<div class="form-group">
		{!! Form::label('level','Level:') !!}
		{!! Form::select('level', array('1' => 'Easy', '2' => 'Middle', '3'=>'Hard'), null, ['placeholder' => 'Pick a level...']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('room', 'Room:') !!}
		{!! Form::select('room', array(
    'A Blcok' => array('a101' => 'A101','a102' => 'A102','a103' => 'A103','a104' => 'A104','a105' => 'A105'),
    'B Blcok' => array('b101' => 'B101','b102' => 'B102','b103' => 'B103','b104' => 'B104','b105' => 'B105'),
    'C Blcok' => array('c101' => 'C101','c102' => 'C102','c103' => 'C103','c104' => 'C104','c105' => 'C105'),
    'D Blcok' => array('d101' => 'D101','d102' => 'D102','d103' => 'D103','d104' => 'D104','d105' => 'D105'),
)) !!}
	</div>
	<div class="form-group">
		{!! Form::label('weekDay', 'Week Day:') !!}
		{!! Form::text('weekDay', $_GET['week'], array('class'=> 'form-control', 'readonly')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('time', 'Time:') !!}
		{!! Form::text('time', $_GET['time'] . ":00:00", array('class'=> 'form-control', 'readonly')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('duration','Class Duration:') !!}
		{!! Form::select('duration', array('30' => 'Half Hour', '60' => 'OneHour'), null, ['placeholder' => 'Pick a level...']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('tuitionFee', 'Tuition Fee:') !!}
		{!! Form::text('tuitionFee', $_GET['tuitionFee'], array('class'=> 'form-control', 'readonly')) !!}
	</div>
		{{ Form::hidden('teacherNum', $_GET['teacherNum']) }}
	{!! Form::token() !!}
	{!! Form::submit(null,array('class'=> 'btn btn-default')) !!}
	{!! Form::close() !!}
	</div>
	</div>
</div>
@endsection