@extends('layouts.master')

@section('content')

    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <div class="navbar-brand">Piano Lessons</div>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="#">Link 1</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<div class="container">
		<div class="row">
	        <div class="col-lg-12 jumbotron">
				<div id="photopianolesson"> <img src="/resources/images/pianolesson.jpg" alt="piano lesson girl"></div>
				<br/>
				<p>We pride ourselves on offering quality music tuition and our piano lessons are no exception.  We offer piano lessons in a range of styles, including Classical, Jazz, Blues and Popular and Contemporary.  If you have a driving passion for music, or are just curious to see how far you can take your piano playing, Sono School of Music will help you every step of the way.  Whether you are a complete beginner, a parent seeking piano lessons for your child or a more advanced player looking to improve further, we can help you to achieve your goals and take you to the level you want to be.</p>
				<p>At Sono School of Music, our our aim is for our clients gain maximum improvement from their piano lessons.  Your piano lessons will be designed to be directly relevant to your playing goals.  We know that this is key to your enjoyment and that enjoying your playing will bring greater engagement with your piano and in turn, better results!</p>
				<p>Our professional piano teachers will strive to help you achieve your goals while you grow as a musician and pianist. All of our teachers are patient, friendly and have the ability to put our students at ease and ready to learn. Play on top quality pianos with experienced, encouraging teachers… request a free piano lesson.</p>
		</div>
		</div>
		<div class="row">
	        <div class="col-lg-12">
				<h2>Lesson Format</h2>
				<p>We offer piano lessons on a weekly or twice-weekly basis with the following appointment options:</p>
				<ul>
					<li>30, 45 or 60 minute sessions.</li>
					<li>15 minutes (for children aged 5 – 6 years only).</li>
				</ul>
				<p>During these sessions we make sure your piano lessons will be designed to be directly relevant to your playing goals.  From years of experience we know this is key to your enjoyment and will bring greater engagement with your piano and in turn, better results!</p>
			</div>
		</div>
		<div class="row">
	        <div class="col-lg-12">
				<h2>Piano Styles</h2>
				<p>We offer piano lessons in all styles, including:</p>
				<ul>
					<li>Classical</li>
					<li>Jazz</li>
					<li>Blues</li>
					<li>Popular & Contemporary</li>
				</ul>
			</div>
		</div>
		<div class="row">
	        <div class="col-lg-12">
				<h2>Program & Examination Tutoring</h2>
				<p>If you wish to study an AMEB program, then our friendly teachers are qualified to help you prepare for your exams and give you the best chance to achieve outstanding results.</p>
				<p>We also provide coaching for other syllabi, such as the Trinity College London, ANZCA and QLD Senior Music and Music Extension.</p>
			</div>
		</div>
		<div class="row">
	        <div class="col-lg-12">
				<h2>Piano Lesson FAQs</h2>
				<h4><i><b>What length of lesson is best?</b></i></h4>
				<p>The improvement that comes with formal music lessons is largely a result of consistent and frequent interaction with your teacher/coach/mentor, and less as a result of the actual musical knowledge that is distributed to you.  For this reason, a weekly 60 minute lesson will be less effective at bringing you improvement than 2 x 30 minute lessons spread across the week.  Similarly, a weekly 60 minute lesson is not likely to yield better results than a weekly 45 minute lesson.  It is questionable that the extra 15 minutes will be of any real benefit, therefore 60 minute private lessons are generally only recommended in special cases.  For the majority of people, we recommend 45 minute lessons.  For younger children, 30 minutes is sufficient where mental fatigue is a consideration.</p>
				<p>Twice-weekly 30 or 45 minute lessons will bring great improvement.</p>
				<p>At Sono School Of Music, we will work to develop a lesson program that will optimise your results.</p>
				<h4><i><b>Do I need to do exams?</b></i></h4>
				<p>No!  We are committed to providing a highly personalised service.  We have the utmost respect for your reasons for playing, your musical preferences and your goals. In some cases, this may mean selecting a syllabus to study, in other cases not. Under guidance from your teacher, the choice is entirely yours. Keep in mind, you can study a syllabus such as AMEB classical piano AND play other things too!</p>
			</div>
		</div>
		<div class="row">
	        <div class="col-lg-12">
				<h2>Class timetable</h2>
				<p>Here is the classes we offer in this season:</p>
				<table class="table table-striped">
					<tr>
						<th></th>
						<th>Moday</th>
						<th>Tuesday</th>
						<th>Wednesday</th>
						<th>Thursday</th>
						<th>Friday</th>
						<th>Saturday</th>
						<th>Sunday</th>
					</tr>
					<tr>
						<td>9:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>10:00</td>
						<td></td>
						<td>
							<a href="{{ URL::route('lesson.enrollment.create', array('class'=>'Piano', 'teacherNum'=>'1', 'teacherName'=>'James', 'week'=>'Tuesday', 'time'=>'10', 'tuitionFee'=>'8888')) }}">
								Classical<br>
								James.
							</a>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>11:00</td>
						<td></td>
						<td></td>
						<td>
							<a href="{{ URL::route('lesson.enrollment.create', array('class'=>'Piano', 'teacherNum'=>'2', 'teacherName'=>'Hannah', 'week'=>'Wednesday', 'time'=>'11', 'tuitionFee'=>'10000')) }}">
								Classical<br>
								Hannah.
							</a>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>12:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>13:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>14:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>15:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>16:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>17:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>18:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<a href="{{ URL::route('lesson.enrollment.create', array('class'=>'Piano', 'teacherNum'=>'2', 'teacherName'=>'Hannah', 'week'=>'Saturday', 'time'=>'18', 'tuitionFee'=>'5000')) }}">
								Classical<br>
								Hannah.
							</a>
						</td>
						<td></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

@endsection