@extends('layouts.master')

@section('content')

    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <div class="navbar-brand" id="musictheoryfirstlink">Music Theory Lessons</div>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
		<li><a href="#lessonformattitle">Lesson Format</a></li>
		<li><a href="#musictheorystylestitle">Music Theory Styles</a></li>
		<li><a href="#programtitle">Program and examination tutoring</a></li>
		<li><a href="#faqtitle">FAQs</a></li>
		<li><a href="#timetabletitle">Timetable</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<div class="container">
		<div class="row" id="top">
	        <div class="col-lg-12 jumbotron">
				<div id="musictheorylesson"> <img src="/resources/images/musictheory.jpg" alt="music theory lesson girl"></div>
				<br/>
				<p>We pride ourselves on offering quality music tuition and our music theory lessons are no exception.  We offer music theory lessons in a range of styles, including Classical, Jazz, Blues and Popular and Contemporary.  If you have a driving passion for music, or are just curious to see how far you can take your music theory playing, Sono School of Music will help you every step of the way.  Whether you are a complete beginner, a parent seeking music theory lessons for your child or a more advanced player looking to improve further, we can help you to achieve your goals and take you to the level you want to be.</p>
				<p>At Sono School of Music, our our aim is for our clients gain maximum improvement from their music theory lessons.  Your music theory lessons will be designed to be directly relevant to your playing goals.  We know that this is key to your enjoyment and that enjoying your playing will bring greater engagement with your music theory and in turn, better results!</p>
				<p>Our professional music theory teachers will strive to help you achieve your goals while you grow as a musician and pianist. All of our teachers are patient, friendly and have the ability to put our students at ease and ready to learn. Play on top quality music theorys with experienced, encouraging teachers… request a free music theory lesson.</p>
		</div>
		</div>
		<div class="row">
	        <div class="col-lg-12">
				<h2 id="lessonformattitle">Lesson Format</h2>
				<p>We offer music theory lessons on a weekly or twice-weekly basis with the following appointment options:</p>
				<ul>
					<li>30, 45 or 60 minute sessions.</li>
					<li>15 minutes (for children aged 5 – 6 years only).</li>
				</ul>
				<p>During these sessions we make sure your music theory lessons will be designed to be directly relevant to your playing goals.  From years of experience we know this is key to your enjoyment and will bring greater engagement with your music theory and in turn, better results!</p>
			</div>
			<br/>
		</div>
		
		<p><a href="#top">Top</a></p>
		
		<div class="row">
	        <div class="col-lg-12">
				<h2 id="musictheorystylestitle">Music theory Styles</h2>
				<p>We offer music theory lessons in all styles, including:</p>
				<ul>
					<li>Classical</li>
					<li>Jazz</li>
					<li>Blues</li>
					<li>Popular & Contemporary</li>
				</ul>
			</div>
		</div>
		
		<br/>
		<p><a href="#top">Top</a></p>
		
		<div class="row">
	        <div class="col-lg-12">
				<h2 id="programtitle">Program & Examination Tutoring</h2>
				<p>If you wish to study an AMEB program, then our friendly teachers are qualified to help you prepare for your exams and give you the best chance to achieve outstanding results.</p>
				<p>We also provide coaching for other syllabi, such as the Trinity College London, ANZCA and QLD Senior Music and Music Extension.</p>
			</div>
		</div>

		<br/>
		<p><a href="#top">Top</a></p>

		<div class="row">
	        <div class="col-lg-12">
				<h2 id="faqtitle">Music theory Lessons FAQs</h2>
				<h4><i><b>What length of lesson is best?</b></i></h4>
				<p>The improvement that comes with formal music lessons is largely a result of consistent and frequent interaction with your teacher/coach/mentor, and less as a result of the actual musical knowledge that is distributed to you.  For this reason, a weekly 60 minute lesson will be less effective at bringing you improvement than 2 x 30 minute lessons spread across the week.  Similarly, a weekly 60 minute lesson is not likely to yield better results than a weekly 45 minute lesson.  It is questionable that the extra 15 minutes will be of any real benefit, therefore 60 minute private lessons are generally only recommended in special cases.  For the majority of people, we recommend 45 minute lessons.  For younger children, 30 minutes is sufficient where mental fatigue is a consideration.</p>
				<p>Twice-weekly 30 or 45 minute lessons will bring great improvement.</p>
				<p>At Sono School Of Music, we will work to develop a lesson program that will optimise your results.</p>
				<h4><i><b>Do I need to do exams?</b></i></h4>
				<p>No!  We are committed to providing a highly personalised service.  We have the utmost respect for your reasons for playing, your musical preferences and your goals. In some cases, this may mean selecting a syllabus to study, in other cases not. Under guidance from your teacher, the choice is entirely yours. Keep in mind, you can study a syllabus such as AMEB classical music theory AND play other things too!</p>
			</div>
			
		</div>
		
		<br/>
		<p><a href="#top">Top</a></p>

		<div class="row">
	        <div class="col-lg-12">
				<h2 id="timetabletitle">Class timetable</h2>
				<p>Here is the classes we offer in this season:</p>
				<table class="table table-striped">
					<tr>
						<th></th>
						<th>Moday</th>
						<th>Tuesday</th>
						<th>Wednesday</th>
						<th>Thursday</th>
						<th>Friday</th>
						<th>Saturday</th>
						<th>Sunday</th>
					</tr>
					<tr>
						<td>9:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>10:00</td>
						<td></td>
						<td>
							<a href="{{ URL::route('lesson.enrollment.create', array('class'=>'Music Theory', 'teacherNum'=>'1', 'teacherName'=>'James', 'week'=>'Tuesday', 'time'=>'10', 'tuitionFee'=>'8888')) }}">
								Classical<br>
								James.
							</a>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>11:00</td>
						<td></td>
						<td></td>
						<td>
							<a href="{{ URL::route('lesson.enrollment.create', array('class'=>'Music Theory', 'teacherNum'=>'2', 'teacherName'=>'Hannah', 'week'=>'Wednesday', 'time'=>'11', 'tuitionFee'=>'10000')) }}">
								Classical<br>
								Hannah.
							</a>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>12:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>13:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>14:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>15:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>16:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>17:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>18:00</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<a href="{{ URL::route('lesson.enrollment.create', array('class'=>'Music Theory', 'teacherNum'=>'2', 'teacherName'=>'Hannah', 'week'=>'Saturday', 'time'=>'18', 'tuitionFee'=>'5000')) }}">
								Classical<br>
								Hannah.
							</a>
						</td>
						<td></td>
					</tr>
				</table>
			</div>
			
		</div>
		<br/>
			<p><a href="#top">Top</a></p>
	</div>

@endsection