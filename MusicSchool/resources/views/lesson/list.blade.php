@extends('layouts.master')

@section('content')

    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="home">Lesson we are offering</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav" role="tablist">
	          <li><a href="profile">Link 1</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<div class="container">
 		
 			<h1 class="bs-docs-featurette-title" > Lesson we are offering</h1>
 			<br><br><br>
		<div class="row" id="accordion">
<hr class="half-rule">
			
	        <div class="col-sm-4">
	        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
	       		 <image src="/resources/images/piano-4.png" class="img-responsive">
	        		<h2 class="panel-title">
	        			<a href="/lesson/classespiano" role="button">Piano Lessons
	        		</h2>
			</a>

	          
	          		<p class="panel-body">We pride ourselves on offering quality music tuition and our piano lessons are no exception.  We offer piano lessons in a range of styles, including Classical, Jazz, Blues and Popular and Contemporary.  If you have a driving passion for music, or are just curious to see how far you can take your piano playing, Sono School of Music will help you every step of the way.  Whether you are a complete beginner, a parent seeking piano lessons for your child or a more advanced player looking to improve further, we can help you to achieve your goals and take you to the level you want to be.<br><br><br></p>
	          
	       
	        </div>


	        <div class="col-sm-4">
	       
	      <image src="/resources/images/microphone-4.png" class="img-responsive">
	          	<h2 class="panel-title">
	          		<a href="/lesson/singingclass" role="button">
	          			Singing Lessons
	          		</a>
	          	</h2>
	         

	         
	          <p class="panel-body">Singing lessons can be challenging yet immensely satisfying for everyone.  Unlike other musical instruments that can be seen and touched, the human voice is far less tangible. You cannot see or touch it but once you learn how to control it, there’s no stopping you.  When you start, you will find that there is always something new to learn about your voice.  And it’s not only beginners who benefit from singing lessons.  Even professional singers continue taking lessons with a voice coach throughout their careers.<br><br><br></p>
	        
	       </div>


	        <div class="col-sm-4">
	         
	         	 <image src="/resources/images/drum-set-1.png" class="img-responsive">
	         	 <a href="/lesson/classesdrums" role="button">
	          	<h2 class="panel-title">Drum Lessons</h2> 
	          </a>
	          		<p class="panel-body">Whether you are a beginner or an experienced drummer looking to expand your beats and fills, Sono School of Music has the drum lessons you are looking for.  Our professional drum teachers will strive to help you achieve your goals as you grow as a musician and drummer.  In addition to being capable and experienced, our drum teachers are patient and friendly and know how to help you feel positive, comfortable and ready to learn.<br><br><br>
	         		 
	          		</p>
	          	</div>


	          		        <div class="col-sm-4">
	          		        
	           <image src="/resources/images/pentagram.png" class="img-responsive">
	          	<h2 class="panel-title">
	          		 <a href="/lesson/classesmusictheory" role="button">
	          			Music Theory Lessons
	          		</a>
	          	</h2> 
	         
	          	          
	          		<p class="panel-body">We offer music theory lessons for graded music theory and musicianship exams.  Please contact us for details.<br><br><br>
	         		
	          		</p>

	          	

	        </div>
	    </div>
	
	</div>

@endsection