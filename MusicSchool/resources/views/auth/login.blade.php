@extends('layouts.master')

@section('content')
<div class="container">
            <div class="panel panel-default" style="width:300px; margin-left: auto; margin-right: auto;">
                <div class="panel-heading"><h2>Login</h2> </div>

                    <div class="panel-body">
    @if(count($errors))
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(array('route'=>'handleLogin', 'method' => 'post')) !!}
        <div class="form-group">
            {!! Form::label('email') !!}
            {!! Form::text('email',null, array('class'=> 'form-control')) !!}
        </div>
        <div class="form-group">
            {!! Form::label('password') !!}
            {!! Form::password('password', array('class'=> 'form-control')) !!}
        </div>
        {!! Form::token() !!}
        {!! Form::submit("Login",array('class'=> 'btn btn-default')) !!}
    {!! Form::close() !!}
                </div>
            </div>
   
</div>
@endsection