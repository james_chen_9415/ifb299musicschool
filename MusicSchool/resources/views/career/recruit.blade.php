@extends('layouts.master')

@section('content')

    <nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="home">Current Employment Opportunities</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="profile">Link 1</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
<div class="container">
	<h1>Pinelands Music School is Hiring!</h1>
	<p>Sono is seeking to hire fantastic people to work with our ever growing team in order to meet increased demand for our coaching services in:</p>
	<p>
		<ul>
			<li>vocal coach, full time</li>
			<li>vocal coach, part-time (hours by negotiation)</li>
 		</ul>
 	</p>
 	<p>For the right candidate (the best of the best), this job will be the opportunity of a lifetime. You will work closely with the Sono Team, learn something new each day, and perform work that impacts the lives of our many clients and the wider community.</p>

	<h2>Benefits of This Role</h2>
	<p>This is a hands-on role. You will need to hustle and work hard. If you’re up for that, there are a lot of benefits.</p>
	<ol>
		<li><b>You will perform work that matters.</b> Pinelands Music School was founded on the philosophy that anyone can learn to play music.  We are an all inclusive facility for people to develop their musicianship regardless of age, ability or ambition.  Your work will influence the lives of many people.  Not only will you have a direct influence on our clients (that will last for their entire life!), but you will also influence the people our clients interact with, and the people those people interact with.  You will have a duty to promote positive, evidence based information about developing musicianship in the hope of making right the atrocities committed by abdicative music teachers past and present.</li>
		<li><b>You will learn constantly.</b> This is a job that will force you to grow. You’ll learn something new and be challenged pretty much every day. It is our goal for you to be better at the end of each year than you were at the beginning of it.</li>
		<li><b>You will become a better musician.</b>  Our team members help each other, interact, play together, and learn from each other.  You will have at your disposal our fantastic coaches across a range of instruments!</li>
		<li><b>You will be well paid.</b> Your compensation will be competitive. It is important to me that you are compensated fairly (based on the value you bring to the business, your experience and skills) and that this job moves you in the direction that you want your life and career to be going.</li>
		<li><b>You can co-create your job.</b> As we develop a working relationship and you become comfortable with our style and expectations, we want you to think about how your skills and interests align with the mission of our company. Part of your job will be to design a role that makes you excited to come to work each day and fully leverages your strengths.</li>
		<li><b>You can advance your career.</b> For the right person, this can become a leadership role in the future. Future promotions are not promised in any way, but top performers will be in prime position to advance.</li>
	</ol>

	<h2>Requirements For This Position</h2>
	<p>We’re very open to hiring candidates with different backgrounds, experiences, and skills. However, there are a few non-negotiable traits that any successful candidate will possess.</p>
	<ol>
		<li><b>Your default setting is to be proactive not reactive.</b> You ask questions rather than letting topics go undiscussed. You take ownership of problems and develop solutions without being asked. To do this job well, you need to be eager to take action and have a history of getting things done.</li>
		<li><b>You have fantastic interpersonal and communication skills.</b> This job will require you to be a highly astute communicator.  You must be able to listen.  You must be able to “read” people and understand they might be thinking one thing and telling you another.  You must be able to speak clearly and persuasively.  You must be able to make people feel comfortable with giving you the honest truth.</li>
		<li><b>You are punctual.</b>  It goes without saying, however many people struggle to get to work on time!</li>
		<li><b>You are very comfortable with technology.</b> Nearly everything we use or create requires some type of software program. You don’t need to be a programmer, but you do need to have fluency with technology and feel comfortable with using technology.</li>
		<li><b>You bring energy into the room rather than take energy out of it.</b> Great teams cannot thrive when people act selfishly, lack ambition, or otherwise row in the opposite direction as the rest of the boat. We’re looking for someone who is fully “on board” with our mission, brings great energy and enthusiasm each day, and has a genuine desire to help others and be part of a team.</li>
		<li><b>You constantly seek to improve yourself.</b>  You will have an open mind and always be looking for ways to do things better.  You will accept and act upon advice.  You will maintain regular, formal music lessons as you are committed to continually improving your musicianship.  You practice what you preach.</li>
		<li><b>You are a professional person.</b>  Professional people do not make decisions that compromise their professional relationships.  They do not double book themselves, prioritise social events over work, or otherwise cause unnecessary inconvenience to their clients.  As a professional person you can successfully manage other engagements with this one. </li>
		<li><b>You are committed to a career as a music coach.</b>  We only seek people who know this is what they want to do in life.  This isn’t just a job. </li>
	</ol>

	<h2>Interested?</h2>
	<form method="get" action="/register">
		<input type="submit" value="APPLY" class="btn btn-default" />
	</form>
	<p>&nbsp;</p>
	</div>
@endsection