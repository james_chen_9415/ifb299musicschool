<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentEditProfileTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(){
        parent::setUp();

        // register first for further test
        $this->visit('/users/create')
            ->select('student','role')
            ->type('james','firstName')
            ->type('chen','lastName')
            ->type('jamestutorjamestutor@gmail.com','email')
            ->type('password123456','password')
            ->type('password123456','password_confirmation')
            ->press('Register');
        $this->visit('/login')
            ->type('jamestutorjamestutor@gmail.com','email')
            ->type('password123456','password')
            ->press('Login');
    }
    public function tearDown(){
        parent::tearDown();
        Mockery::close();
    }

    /**
     * Test to go to profile page
     *
     * @test
     */
    public function testEditProfile()
    {
        $this->click('My Profile')
            ->press('Edit')
            //->seePageIs('/profile/edit')
            ->type('myfirstName','firstName')
            ->type('mylastName','lastName')
            ->type('0414656611', 'phoneNum')
            ->type('1991-11-09', 'DOB')
            ->type('QUT GP', 'address')
            ->type('myFacebookId', 'FacebookId')
            ->press('Submit');
        $this//->seePageIs('/profile/edit')
        ->press('Edit')
            ->see('myfirstName')
            ->see('mylastName')
            ->see('0414656611')
            ->see('1991-11-09')
            ->see('QUT GP')
            ->see('myFacebookId');
    }

}
