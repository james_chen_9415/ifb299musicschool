<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EnrollmentTest extends TestCase
{
	use DatabaseTransactions;
	
	/**
     * A basic functional test example.
     *
     * @return void
	 * @test
     */
    public function EnrollmentSequenceTest()
    {
		//Register
		
        $this->visit('/users/create')
            ->select('student','role')
            ->type('james','firstName')
            ->type('chen','lastName')
            ->type('jamesStudentStudent@gmail.com','email')
            ->type('password123456','password')
            ->type('password123456','password_confirmation')
            ->press('Register');
        $this->seePageIs('/profile/edit');
		
		//Edit profile
        $this->click('My Profile')
            ->press('Edit')
            ->type('myfirstName','firstName')
            ->type('mylastName','lastName')
            ->type('0414656611', 'phoneNum')
            ->type('1991-11-09', 'DOB')
            ->type('QUT GP', 'address')
            ->type('myFacebookId', 'FacebookId')
            ->press('Submit');
        $this->press('Edit')
            ->see('myfirstName')
            ->see('mylastName')
            ->see('0414656611')
            ->see('1991-11-09')
            ->see('QUT GP')
            ->see('myFacebookId');
		//Enrol
		$this->click('Lessons')
			->see('Piano Lessons')
			->see('Singing Lessons')
			->see('Drum Lessons')
			->see('Music Theory Lessons');
		$this->click('Piano Lessons')
			->see('PIANO Lessons');

    }
}