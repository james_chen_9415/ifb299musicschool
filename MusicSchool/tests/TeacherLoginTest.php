<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TeacherLoginTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(){
        parent::setUp();

        // register first for further test
        $this->visit('/users/create')
            ->select('teacher','role')
            ->type('james','firstName')
            ->type('chen','lastName')
            ->type('jamestutorjamestutor@gmail.com','email')
            ->type('password123456','password')
            ->type('password123456','password_confirmation')
            ->press('Register');
    }
    public function tearDown(){
        parent::tearDown();
        Mockery::close();
    }

    /**
     * A correct login test.
     *
     * @return void
     * @test
     */
    public function testCorrectLogin()
    {
        $this->visit('/login')
            ->type('jamestutorjamestutor@gmail.com','email')
            ->type('password123456','password')
            ->press('Login');
        $this->seePageIs('/home')
            ->see('Welcome, james');
    }

    /**
     * A wrong password login test.
     *
     * @return void
     * @test
     */
    public function testWrongPassword()
    {
        $this->visit('/login')
            ->type('jamestutorjamestutor@gmail.com','email')
            ->type('password12','password')
            ->press('Login');
        $this->seePageIs('/login')
            ->see('Email or password is invalid');
    }

    /**
     * A wrong password login test.
     *
     * @return void
     * @test
     */
    public function testNotExistEmail()
    {
        $this->visit('/login')
            ->type('jamestutorjamestutor23456789@gmail.com','email')
            ->type('password123456','password')
            ->press('Login');
        $this->seePageIs('/login')
            ->see('The selected email is invalid.');
    }
}
