<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MyPotalTest extends TestCase
{
    use DatabaseTransactions;
	
	public function setUp(){
        parent::setUp();

        // register first for further test
        $this->visit('/users/create')
            ->select('student','role')
            ->type('james','firstName')
            ->type('chen','lastName')
            ->type('jamestutorjamestutor@gmail.com','email')
            ->type('password123456','password')
            ->type('password123456','password_confirmation')
            ->press('Register');
    }
    public function tearDown(){
        parent::tearDown();
        Mockery::close();
    }
	/**
     * A correct login test.
     *
     * @return void
     * @test
     */
	 
	public function testMyPotal()
	{
		$this->visit('/admin')
			->see('Enrolled Classes')
			->see('Lesson Type')
			->see('Start Date')
			->see('End Date') 
			->see('Start time')
			->see('WeekDay')
			->see('Tuition Fee')
			->click('Hire instrument');
		$this->seePageIs('/student/hireInstrument')
			->see('Choose your instrument')
			->select('All','instrument-filter')
			->see('instrument')
			->see('ID')
			->see('info')
			->see('Cost per month')
			->see('Select');
		$this->select('Guitar','instrument-filter')
			->select('Cello','instrument-filter')
			->select('Piano','instrument-filter')
			->select('Percussion','instrument-filter');
		$this->select('Order by','orderby')
			->select('instrument','orderby')
			->select('cost','orderby'); 

	}
}
