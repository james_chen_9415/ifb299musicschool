<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TeacherApplyTest extends TestCase
{
	use DatabaseTransactions;
	
	/**
	 * The function is to test that users cannot register 
	 * by the same email.
	 *
	 * @test
	*/
    public function applyingTest()
    {
		$this->visit('/')
			 ->click('Careers')
			 ->see('Interested?')
			 ->press('APPLY');
		$this->see('Register')
			 ->select('teacher','role')
			 ->type('firstName','firstName')
			 ->type('lastName','lastName')
			 ->type('0987654321@gmail.com','email')
			 ->type('password','password')
             ->type('password','password_confirmation')
			 ->press('Register');
		$this->visit('/profile/edit')
			->type('dong','firstName')
            ->type('Choi','lastName')
            ->type('123123123', 'phoneNum')
            ->type('1991-11-09', 'DOB')
            ->type('QUT GP', 'address')
            ->type('English', 'language')
            ->type('myFacebookId', 'FacebookId')
            ->press('Submit');
 		$this->visit('/profile/edit')
			->see('email')
			->see('firstName')
            ->see('lastName')
			->see('gender')
			->see('phoneNum')
            ->see('1991-11-09')
			->see('address')
            ->see('QUT GP')           
            ->see('myFacebookId')
			->see('English');	
	    $this->visit('/lesson/apply')
			 ->see('teacherNum')
			 ->see('email')
			 ->see('firstName')
			 ->see('lastName')
			 ->type('skill','skill');
	 			 
	}
	
}
