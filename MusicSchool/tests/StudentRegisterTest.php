<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentRegisterTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * The function is to test that users cannot register
     * by the same email.
     *
     * @test
     */
    public function duplicationRegisterTest()
    {
        $this->visit('/users/create')
            ->select('student','role')
            ->type('firstName','firstName')
            ->type('lastName','lastName')
            ->type('1234@gmail.com','email')
            ->type('password','password')
            ->type('password','password_confirmation')
            ->press('Register');
        $this->see('The email has already been taken.')
            ->seePageIs('/users/create');
    }

    /**
     * The function is to test that users can register by the email
     *
     * @test
     */
    public function studentCorrectRegisterTest()
    {
        $this->visit('/users/create')
            ->select('student','role')
            ->type('james','firstName')
            ->type('chen','lastName')
            ->type('jamesStudentStudent@gmail.com','email')
            ->type('password123456','password')
            ->type('password123456','password_confirmation')
            ->press('Register');

        // pass to register and redirect to edit profile
        $this->seePageIs('/profile/edit');
    }

    /**
     * The function is to test that users cannot register name including numbers
     *
     * @test
     */
    public function nameIncludingNumRegisterTest()
    {
        $this->visit('/users/create')
            ->select('student','role')
            ->type('james12342','firstName')
            ->type('chen53645','lastName')
            ->type('jamestutorjamestutor@gmail.com','email')
            ->type('password123456','password')
            ->type('password123456','password_confirmation')
            ->press('Register');
        $this->see("The first name may only contain letters.")
            ->seePageIs('/users/create');
    }

    /**
     * The function is to test that users cannot register if they type two different passwords
     *
     * @test
     */
    public function confirmPasswordWrongRegisterTest()
    {
        $this->visit('/users/create')
            ->select('student','role')
            ->type('james12342','firstName')
            ->type('chen53645','lastName')
            ->type('jamestutorjamestutor@gmail.com','email')
            ->type('password123456','password')
            ->type('password156','password_confirmation')
            ->press('Register');
        $this->see("The password confirmation does not match.")
            ->seePageIs('/users/create');
    }
}
